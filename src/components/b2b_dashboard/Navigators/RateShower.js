import React from 'react'
import { Col, Icon, Row } from 'rsuite'

function RateShower(props) {
    return (
        <div className='rates'>
                   <Row>
                      <Col xs={8} sm={8} md={8} lg={8}>
                            <div className='usd_value'>1</div>
                       </Col>
                       <Col xs={8} sm={8} md={8} lg={8}>
                           <div className='sell_label'>SELL</div>
                       </Col>
                       <Col xs={8} sm={8} md={8} lg={8}>
                           <div className='buy_label'>BUY</div>
                       </Col>
                   </Row>
           {
               props.data.map((r,i)=>(
                   <Row key={i}>
                       <Col xs={8} sm={8} md={8} lg={8}>
                         <div className='usd_label'>USD</div>
                       </Col>
                       <Col xs={8} sm={8} md={8} lg={8}>
                            <div className='sell_value'>{r.sell} </div>
                       </Col>
                       <Col xs={8} sm={8} md={8} lg={8}>
                           <div className='buy_value'>{r.buy} </div>
                       </Col>
                   </Row>
               ))
           }
        </div>
    )
}

export default RateShower
