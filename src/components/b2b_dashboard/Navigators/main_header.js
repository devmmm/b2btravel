import React from 'react'
import {  Icon, Navbar, Nav, Dropdown } from 'rsuite'
import { useHistory } from 'react-router-dom'
import RateShower from './RateShower';

function MainHeader(props) {
    const history = useHistory()
    const show = props.show;
    const setshow = props.setShow;
    const rates = props.rates;
    const system_Logout = () =>{
        localStorage.clear();
        history.push('/')
    }
    return (
        <div>
             <Navbar appearance='default' style={{background:'#f2f2f2'}}>
                <Navbar.Header>
                   <Nav>
                       <Nav.Item  onClick={()=>{setshow(!show)}} icon={<Icon icon='dedent' />}>B2B.TRAVELS.COM.MM</Nav.Item>
                   </Nav>
                </Navbar.Header>
                <Navbar.Body>
                <Nav pullRight>
                <Dropdown  icon={<Icon icon='user' />} title={props.username}>
              
                <Dropdown.Item onClick={()=>{system_Logout()}} icon={<Icon icon='power-off' />}>Logout</Dropdown.Item>
                </Dropdown>
                </Nav>
                <Nav pullRight>
                   <RateShower data={rates} />
                </Nav>
                </Navbar.Body>
             </Navbar>
        </div>
    )
}

export default MainHeader
