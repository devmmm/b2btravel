import React from 'react'
import { Drawer, Sidenav, Nav, Icon,Dropdown } from 'rsuite';
import { theme } from '../../../tools/theme';
import { useHistory } from 'react-router-dom';

function MainSideBar(props) {
    const show = props.show;
    const setshow = props.setShow;
    const usermodules = props.userModules;
    const history = useHistory()
const IconName = (module) =>{
 if(module==='BUS'){
     return 'bus'
 }
 else if(module==='MOBILE'){
     return 'mobile'
 }
 else if(module==='FLIGHT'){
     return 'plane'
 }
}

function renderMenu (um,i){
    if(um.moduleName==='BUS'){
       return(
        <>
        <Dropdown key={i} eventKey={`2+${i}`} title={um.moduleName} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/`)}}   icon={<Icon icon={IconName(um.moduleName)} />}>
        <Dropdown.Item eventKey={`2+${i}-${i+1}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/bus_search`);setshow(!show)}} icon={<Icon icon={IconName(um.moduleName)} />} >{um.moduleName}  Search </Dropdown.Item>
        <Dropdown.Item eventKey={`2+${i}-${i+2}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/booking/booking_list`);setshow(!show)}} icon={<Icon icon='bookmark-o' />} >{um.moduleName}  Bookings </Dropdown.Item>
        </Dropdown> 
       </>
       )
    }
    else if(um.moduleName==='FLIGHT'){
        return(
            <>
            <Dropdown key={i} eventKey={`2+${i}`} title={um.moduleName} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/`)}} icon={<Icon icon={IconName(um.moduleName)} />}>
            <Dropdown.Item eventKey={`2+${i}-${i+1}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/flight_search`);setshow(!show)}} icon={<Icon icon={IconName(um.moduleName)} />} >{um.moduleName}  Search </Dropdown.Item>
            <Dropdown.Item eventKey={`2+${i}-${i+2}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/flight_bookings`);setshow(!show)}} icon={<Icon icon='bookmark-o' />} >{um.moduleName}  Bookings </Dropdown.Item>
            <Dropdown.Item eventKey={`2+${i}-${i+3}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/flight_order_offline_booking`);setshow(!show)}} icon={<Icon icon={IconName(um.moduleName)} />} >  {um.moduleName}  Order Offline Booking </Dropdown.Item>
            <Dropdown.Item eventKey={`2+${i}-${i+4}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/flight_offline_bookings_list`);setshow(!show)}} icon={<Icon icon='bookmark-o' />} >{um.moduleName}  Offline Bookings </Dropdown.Item>
            </Dropdown> 
           </>
           )
    }
    else if(um.moduleName==='MOBILE'){
        return(
            <>
            <Dropdown key={i} eventKey={`2+${i}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/`)}} title={um.moduleName} icon={<Icon icon={IconName(um.moduleName)} />}>
            <Dropdown.Item eventKey={`2+${i}-${i+1}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/`);setshow(!show)}} icon={<Icon icon={IconName(um.moduleName)} />} >{um.moduleName}   </Dropdown.Item>
            <Dropdown.Item eventKey={`2+${i}-${i+2}`} onClick ={()=>{history.push(`/dashboard/${um.moduleURL}/transactions`);setshow(!show)}} icon={<Icon icon='history' />} >{um.moduleName}  Transactions </Dropdown.Item>
            
            </Dropdown> 
           </>
           )
    }
}
    return (
        <div>
            <Drawer style={{fontFamily:theme.default.fontfamily,maxWidth:250}} show={show} onHide={()=>{setshow(!show)}} placement='left' size='xs' >
                <Drawer.Body>
                    <Sidenav appearance='subtle'>
                     <Sidenav.Body>
                         <Nav>
                             <Nav.Item eventKey='1' icon={<Icon icon='dashboard' />} > Dashboard </Nav.Item>
                             <Dropdown eventKey="2" title="Business Entry" icon={<Icon icon='industry' />}>
                             <Dropdown.Item onClick ={()=>{history.push(`/dashboard/business_entry/`);setshow(!show)}} eventKey="2-3" icon={<Icon icon='profile' />} >Profile</Dropdown.Item>
                                <Dropdown.Item onClick ={()=>{history.push(`/dashboard/business_entry/balance`);setshow(!show)}} eventKey="2-1" icon={<Icon icon='money' />} >Balance</Dropdown.Item>
                                <Dropdown.Item onClick ={()=>{history.push(`/dashboard/business_entry/exchange`);setshow(!show)}} eventKey="2-2" icon={<Icon icon='stack-exchange' />} >Exchange</Dropdown.Item>
                                
                                <Dropdown.Item onClick ={()=>{history.push(`/dashboard/business_entry/transactions`);setshow(!show)}} eventKey="2-4" icon={<Icon icon='history' />} >Transactions</Dropdown.Item>
                            </Dropdown>
                            {
                                usermodules.map((um,i)=>(
                                  <div key={i}>
                                     { renderMenu(um,i) }
                                  </div>
                                ))
                            }
                         </Nav>
                     </Sidenav.Body>
                    </Sidenav>
                </Drawer.Body>
            </Drawer>
        </div>
    )
}

export default MainSideBar
