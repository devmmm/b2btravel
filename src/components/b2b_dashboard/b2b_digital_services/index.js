import React from 'react'
import { Switch, Route } from 'react-router-dom'
import B2BMobile from './digital_services'
import Transactions from './transactions'

function DigitalServices(props) {
    return (
        <div>
            <Switch>
                <Route exact path={`${props.route}/`} component={()=>{return <B2BMobile userData={props.userData} />}} />
                <Route exact  path={`${props.route}/transactions`} component={Transactions} />
            </Switch>
        </div>
    )
}

export default DigitalServices
