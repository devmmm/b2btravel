import React, { useState,useEffect } from 'react'
import { Form, InputGroup, ControlLabel, Input, Avatar, FormGroup, RadioGroup, Radio, Button, Modal, Notification } from 'rsuite'
import { operator_name } from './helpers'
import {check_token,refresh_token} from '../../../tools/helpers'
import { b2b_user_data } from '../../../tools/constants'
import { DS_API } from '../../../tools/api'
import {theme} from '../../../tools/theme/'
function Topup(props) {
    let setMobilenumber=props.setMobilenumber 
    let mobilenumber=props.mobilenumber
    const [amount, setAmount] = useState('1000')
    const [code, setCode] = useState('')
    const [loading, setLoading] = useState(false)
    const [show, setShow] = useState(false)
    const [remark, setRemark] = useState('')
    
    useEffect(() => {
        let telname = operator_name(mobilenumber)
        if(telname==='mpt'){
            setCode('MPTeLoad')
        }
        else if(telname==='telenor'){
          setCode('TelenoreLoad')
      }
     else if(telname==='ooredoo'){
          setCode('OoredooeLoad')
      }
     else if(telname==='mytel'){
          setCode('MyteleLoad')
      }
     else if(telname==='mectel'){
          setCode('MecTeleLoad')
      }
      else{
          setCode('')
      }
      }, [mobilenumber])

      const check_topup = () =>{
        let data = new FormData();
        setLoading(true)
        data.append('mobileno', mobilenumber);
        data.append('rechargeamount', amount);
        data.append('BillerCode', code);
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
                DS_API(b2b_user_data.access_token).post('/mobile/rechargeenquiry',data)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    setShow(true)
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })
            }
            else {
                if(refresh_token(b2b_user_data.refresh_token_old)){
                    DS_API(b2b_user_data.access_token).post('/mobile/rechargeenquiry',data)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                        setShow(true)
                    })
                    .catch(err=>{
                        console.log(err)
                        setLoading(false)
                    })
                }
            }
        }
      }
      const confirmTopup = () =>{
          
          let data = new FormData();
            setLoading(true)
            data.append('mobileno', mobilenumber);
            data.append('rechargeamount',amount);
            data.append('BillerCode', code);
            data.append('remark', remark);
            if(b2b_user_data.access_token!==null){
                if(check_token(b2b_user_data.create_time)){
                    DS_API(b2b_user_data.access_token).post('/mobile/rechargeconfirm',data)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                        if(res.data.Status.Code==='01'){
                           Notification.success({
                               title:'TOPUP',
                               description:`${mobilenumber} is successfully recharged amount ${amount}`,
                               style:{fontFamily:theme.default.fontfamily},
                               
                           })
                        }
                        else{
                            Notification.error({
                                title:'TOPUP',
                                description:"Fail!",
                                style:{fontFamily:theme.default.fontfamily},
                                
                            })
                        }
                        setShow(false)
                    })
                    .catch(err=>{
                        console.log(err)
                        setLoading(false)
                    })
                }
                else {
                    if(refresh_token(b2b_user_data.refresh_token_old)){
                        DS_API(b2b_user_data.access_token).post('/mobile/rechargeconfirm',data)
                        .then(res=>{
                            console.log(res.data)
                            setLoading(false)
                            setShow(false)
                        })
                        .catch(err=>{
                            console.log(err)
                            setLoading(false)
                        })
                    }
                }
            }

      }
    return (
        <div style={{marginTop:10,marginBottom:10}}>
            <Form>
                <InputGroup>
                    <InputGroup.Addon>
                        <ControlLabel>Mobile Number:</ControlLabel>
                    </InputGroup.Addon>
                    <Input value={mobilenumber} onChange={(v)=>{setMobilenumber(v)}} />
                    <InputGroup.Addon>
                     <Avatar src={code==='MPTeLoad'?props.mpt:code==='TelenoreLoad'?props.telenor:code==='OoredooeLoad'?props.ooredoo:code==='MyteleLoad'?props.mytel:null} />
                    </InputGroup.Addon>
                </InputGroup>
                <FormGroup>
                    <ControlLabel>Amount</ControlLabel>
                    <RadioGroup inline value={amount} onChange={(v)=>{setAmount(v)}} >
                        <Radio value='1000'>1000</Radio>
                        <Radio value='3000'>3000</Radio>
                        <Radio value='5000'>5000</Radio>
                        <Radio value='10000'>10000</Radio>
                    </RadioGroup>
                </FormGroup>
                <FormGroup style={{marginTop:10,marginBottom:10}}>
                 <Button loading={loading} onClick={()=>{check_topup()}} disabled={code==='' ? true:false} appearance='primary' color='green'>Continue</Button>
                </FormGroup>
            </Form>
            <Modal size='xs' show={show} style={{fontFamily:theme.default.fontfamily}}>
                <Modal.Title>TOPUP </Modal.Title>
                <Modal.Body>
                 <p> Mobile Number - <small style={{color:'tomato'}}>{mobilenumber} </small> </p>
                 <p> Biller Code - <small style={{color:'tomato'}}>{code} </small> </p>
                 <p> Amount - <small style={{color:'tomato'}}>{amount} </small> </p>
                 <FormGroup>
                     <ControlLabel>Remark</ControlLabel>
                     <Input value={remark} onChange={(v)=>{setRemark(v)}} />
                 </FormGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button  onClick={()=>{setShow(!show)}} appearance='primary' color='yellow'>Cancel</Button> <Button disabled={remark===''?true:false} loading={loading} onClick={()=>{confirmTopup()}} appearance='primary' color='green'>Confrim</Button>
                </Modal.Footer>
            </Modal>
        </div>

    )
}

export default Topup
