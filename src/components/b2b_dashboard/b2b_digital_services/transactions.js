import React, { useState, useEffect } from 'react'
import { Divider,Table, Grid, Row, Col, Panel, Icon } from 'rsuite'
import { b2b_user_data } from '../../../tools/constants'
import { check_token, refresh_token } from '../../../tools/helpers'
import { DS_API } from '../../../tools/api'

function Transactions() {
    const [transactions, setTransactions] = useState([])
    const [loading, setLoading] = useState(false)
    const {Column,HeaderCell,Cell} = Table
    useEffect(() => {
        setLoading(true)
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
                DS_API(b2b_user_data.access_token).get('/mobile/geteloadrecord')
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    setTransactions(res.data.Data)
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)

                })
            }
            else{
                if(refresh_token(b2b_user_data.refresh_token_old)){
                    DS_API(b2b_user_data.access_token).get('/mobile/geteloadrecord')
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                        setTransactions(res.data.Data)
                    })
                    .catch(err=>{
                        console.log(err)
                        setLoading(false)
    
                    })   
                }
            }
        }
    }, [b2b_user_data.create_time])
    return (
        <div>
            <Divider style={{fontWeight:'bold',fontSize:'18px',textTransform:'uppercase'}}>TRANSACTIONS</Divider>
            {
              loading ? 
              <>
               <div className='bus_booking_detial_loader'>
                     <Icon spin icon='spinner' size='2x' />
                  </div>
              </>:
               <Grid>
               <Row>
                   <Col>
                     {
                       transactions.map((t,i)=>(
                         <Panel key={i} className='ds_transaction_div' header={<p className='heading'>{t.orderNo} </p>}>
                          <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='label'> <Icon icon='mobile' /> Mobile Number </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                              <p className='value'> {t.mobileNo}</p>
                            </Col>
                          </Row>
                          <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='label'> <Icon icon='code' /> Operator Code </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='value'>{t.billerCode}</p>
                            </Col>
                          </Row>
                          <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='label'> <Icon icon='money' /> Recharge Amount </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                              <p className='value'> {t.rechargeAmount} MMK</p>
                            </Col>
                          </Row>
                          <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='label'> <Icon icon='money' /> eBARefNo </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='value'>  {t.eBARefNo} </p>
                            </Col>
                          </Row>
                          <Divider />
                          <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='label'>   Status </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                              <p className={t.rechargeStatus==='Success' ? 'status_success':'status_pending'}> {t.rechargeStatus} </p>
                            </Col>
                          </Row>
                          <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='label'> <Icon icon='calendar' />   Date: </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                            <p className='value'>  {t.rechargeTime} </p>
                            </Col>
                          </Row>
                         </Panel>
                       ))
                     }
                   </Col>
               </Row>
           </Grid>
            }
        </div>
    )
}

export default Transactions
