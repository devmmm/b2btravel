import React, { useState, useEffect } from 'react'
import { Form, Panel, InputGroup, ControlLabel, Input, Avatar, Placeholder, Row, Col, Divider,Button, Modal, Notification } from 'rsuite'

import { DS_API } from '../../../tools/api'
import { b2b_user_data } from '../../../tools/constants'
import { check_token, refresh_token } from '../../../tools/helpers'
import { operator_name } from './helpers'
import { theme } from '../../../tools/theme'

function DataPacks(props) {
    let setMobilenumber=props.setMobilenumber 
    let mobilenumber=props.mobilenumber
    const [datapacks, setDatapacks] = useState([])
   
    const [loading, setLoading] = useState(false)
    const [code, setCode] = useState('')
    const [packageCode, setPackageCode] = useState('')
    const [price, setPrice] = useState('')
    const [show, setShow] = useState(false)
    useEffect(() => {
      
        let telname = operator_name(mobilenumber)
      if(telname==='mpt'){
            setCode('MPTDataPack')
        }
       else if(telname==='telenor'){
          setCode('TelenorDataPack')
      }
     else if(telname==='ooredoo'){
          setCode('OoredooDataPack')
      }
     else if(telname==='mytel'){
          setCode('MyTelDataPack')
      }
     
   
   else{
       setCode('')
   }
       console.log(code)
    }, [mobilenumber])


    useEffect(() => {
        var formdata = new FormData();
        formdata.append("billerCode",code);
        setLoading(true)
      if(code!==''){
          if(b2b_user_data.access_token!==null){
              if(check_token(b2b_user_data.create_time)){
                  DS_API(b2b_user_data.access_token).post('/mobile/datapackageenquiry',formdata)
                  .then(res=>{
                      console.log(res.data)
                      setLoading(false)

                  })
                  .catch(err=>{
                      console.log(err)
                  })
              }
              else {
                  if(refresh_token(b2b_user_data.refresh_token_old)){
                    DS_API(b2b_user_data.access_token).post('/mobile/datapackageenquiry',formdata)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
  
                    })
                    .catch(err=>{
                        console.log(err)
                    })  
                  }
              }
          }
      }
    }, [code])

    useEffect(() => {
    if(code!==''){
        var formdata = new FormData();
        formdata.append("billerCode",code);
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
                setLoading(true)
                DS_API(b2b_user_data.access_token).post('/mobile/datapackageenquiry',formdata)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    if(res.data.Data.packageList){
                     setDatapacks(res.data.Data.packageList)
                  }
                  else{
                     setDatapacks(res.data.Data.PackageList)  
                  }
                })
                .catch(err=>{
                    console.log(err)
                })
            }
            else {
                if(refresh_token(b2b_user_data.refresh_token_old)){
                    setLoading(true)
                    DS_API(b2b_user_data.access_token).post('/mobile/datapackageenquiry',formdata)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                        if(res.data.Data.packageList){
                         setDatapacks(res.data.Data.packageList)
                      }
                      else{
                         setDatapacks(res.data.Data.PackageList)  
                      }
                    })
                    .catch(err=>{
                        console.log(err)
                    })
                }
            }
         }
        
        
        else{
            setDatapacks([])
        }
    }
    }, [code])

    const  choosePackage = (p) =>{
        if(p.packageCode){
            setPackageCode(p.packageCode)
        }
        else if(p.OfferName){
            setPackageCode(p.OfferName)
        }
        else {
            setPackageCode(p.Package)
        }
        if(p.price){
            setPrice(p.price)
        }
        else{
            setPrice(p.Price)
        }
       setShow(!show)
     }
    
     const comfirmDataPack = () =>{
        let data = new FormData();
        data.append('billerCode', code);
        data.append('mobileno', mobilenumber);
        data.append('packageCode',packageCode);
        data.append('price', price);
        if(b2b_user_data.access_token !==null){
            if(check_token(b2b_user_data.create_time)){
                setLoading(true)
                DS_API(b2b_user_data.access_token).post('/mobile/datapackageconfirm',data)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    if(res.data.Status.Code==='01'){
                        Notification.success({
                            title:'B2B.TRAVEL.COM.MM',
                            description:`${mobilenumber} is successfully purchaged ${packageCode}`,
                            style:{fontFamily:theme.default.fontfamily}
                        })
                        setShow(!show)
                     }
                     else {
                         Notification.error({
                             title:'B2B.TRAVEL.COM.MM',
                             description:`${mobilenumber} is unsuccessfully purchaged ${packageCode}`,
                             style:{fontFamily:theme.default.fontfamily}
                         })
                         setShow(!show)
                     }
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                    setShow(!show)
                })
            }
            else{
                if(refresh_token(b2b_user_data.refresh_token_old)){
                    setLoading(true)
                    DS_API(localStorage.getItem('access_token')).post('/mobile/datapackageconfirm',data)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                        if(res.data.Status.Code==='01'){
                           Notification.success({
                               title:'B2B.TRAVEL.COM.MM',
                               description:`${mobilenumber} is successfully purchaged ${packageCode}`,
                               style:{fontFamily:theme.default.fontfamily}
                           })
                           setShow(!show)
                        }
                        else {
                            Notification.error({
                                title:'B2B.TRAVEL.COM.MM',
                                description:`${mobilenumber} is unsuccessfully purchaged ${packageCode}`,
                                style:{fontFamily:theme.default.fontfamily}
                            })
                            setShow(!show)
                        }
                    })
                    .catch(err=>{
                        console.log(err)
                        setLoading(false)
                        setShow(!show)
                    })
                }
            }
        
        }
     }
    return (
        <div style={{marginTop:10,marginBottom:10}}>
           <Form>
                   <InputGroup>
                   <InputGroup.Addon> 
                    <ControlLabel>Mobile Number</ControlLabel>
                   </InputGroup.Addon>
                   <Input value={mobilenumber} onChange={(v)=>{setMobilenumber(v)}} />
                   <InputGroup.Addon>
                    <Avatar src={code==='MPTDataPack'?props.mpt:code==='TelenorDataPack'?props.telenor:code==='OoredooDataPack'?props.ooredoo:code==='MyTelDataPack'?props.mytel:null} />
                   </InputGroup.Addon>
                   </InputGroup>
           </Form>
          
          <div style={{marginTop:20,marginBottom:10}}>
              <Divider>{code} </Divider>
          {
               loading ?
               <Row>
                   <Col  xs={24} sm={24} md={6} lg={6}>
                                  <Placeholder.Graph style={{margin:4}}  active height={250} />
                              </Col>  
                              <Col  xs={24} sm={24} md={6} lg={6}>
                                  <Placeholder.Graph style={{margin:4}}  active height={250} />
                              </Col> 
                              <Col  xs={24} sm={24} md={6} lg={6}>
                                  <Placeholder.Graph  style={{margin:4}} active height={250} />
                              </Col> 
                              <Col  xs={24} sm={24} md={6} lg={6}>
                                  <Placeholder.Graph style={{margin:4}}  active height={250} />
                              </Col> 
               </Row>:
               <Row>
                    {
                                  datapacks.map((p,i)=>(
                                      <Col key={i} xs={24} sm={24} md={6} lg={6}>
                                  <Panel  bodyFill style={{marginTop:10,background:'#fff',minHeight:250,textAlign:'center',paddingTop:10}} >
                                       <Avatar src={code==='MPTDataPack'?props.mpt:code==='TelenorDataPack'?props.telenor:code==='OoredooDataPack'?props.ooredoo:code==='MyTelDataPack'?props.mytel:null} size='lg' />
                                      <Panel header={code} style={{textAlign:'center'}} >
                                          <p>{p.packageName?p.packageName:p.Package?p.Package:p.OfferName ?p.OfferName:null}</p>
                                          <p><small>{p.price?p.price:p.Price} MMK </small> </p>
                                          <Button onClick={()=>{choosePackage(p)}} appearance='primary' color='green' size='sm' block>Select</Button>
                                      </Panel>
                                  </Panel>
                              </Col>
                                  ))
                              }
               </Row>
           }
          </div>
          <Modal style={{fontFamily:theme.default.fontfamily}} show={show}>
              <Modal.Title>DATAPACKS</Modal.Title>
              <Modal.Body>
                  <p> Mobile Number -<small> -{mobilenumber}</small> </p>
                  <p> Biller Code - <small> -{code}</small> </p>
                  <p> Package Code - <small> -{packageCode}</small> </p>
                  <p> Price - <small> -{price} MMK </small> </p>
              </Modal.Body>
              <Modal.Footer>
                  <Button appearance='primary' color='orange' onClick={()=>{setShow(!show)}}>Cancel</Button>  <Button loading={loading} onClick={()=>{comfirmDataPack()}} appearance='primary' color='green'>Confirm</Button>
              </Modal.Footer>
          </Modal>
        </div>
    )
}

export default DataPacks
