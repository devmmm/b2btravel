import React, { useState, useEffect } from 'react'
import { Divider, Grid, Row, Panel, Nav, Icon, Col } from 'rsuite'
import Topup from './Topup'
import DataPacks from './DataPacks'
import Epin from './Epin'
import MPT from '../../../assets/mpt.png'
import TELENOR from '../../../assets/telenor.png'
import OOREDOO from '../../../assets/ooredoo.png'
import MYTEL from '../../../assets/mytel.png'


function B2BMobile(props) {
    const [index, setIndex] = useState('1')
    const [mobilenumber, setMobilenumber] = useState(props.userData.businessInfo.businessPhone)
    
   
    return (
        <div>
           <Divider>DIGITAL SERVICES</Divider>
           <Grid>
              <Panel style={{background:'#f2f2f2'}}>
                <Nav justified appearance='subtle' activeKey={index} onSelect={(i)=>{setIndex(i)}} >
                 <Nav.Item eventKey='1' icon={<Icon icon='mobile' />} >Mobile Topup </Nav.Item>
                 <Nav.Item eventKey='2' icon={<Icon icon='database' />} >Data Packs </Nav.Item>
                 {/* <Nav.Item eventKey='3' icon={<Icon icon='file-download' />} > EPIN </Nav.Item> */}
                </Nav>
                <Row>
                    <Col xs={24} sm={24} md={24} lg={24}>
                        {
                            index==='1' ? 
                            <Topup setMobilenumber={setMobilenumber}  mobilenumber={mobilenumber} mpt={MPT} telenor={TELENOR} ooredoo={OOREDOO} mytel={MYTEL} />:null
                        }
                        {
                            index==='2'?
                            <DataPacks setMobilenumber={setMobilenumber} mobilenumber={mobilenumber}  mpt={MPT} telenor={TELENOR} ooredoo={OOREDOO} mytel={MYTEL}  />:null
                        }
                        {
                            index==='3'?
                            <Epin />:null
                        }
                    </Col>
                </Row>
              </Panel>
           </Grid>
           
        </div>
    )
}

export default B2BMobile
