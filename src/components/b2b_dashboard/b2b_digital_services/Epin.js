import React, { useState } from 'react'
import { FormGroup, ControlLabel, RadioGroup, Radio, Input, Button, Modal, Message } from 'rsuite'
import { b2b_user_data } from '../../../tools/constants'
import { DS_API } from '../../../tools/api'
import { check_token, refresh_token } from '../../../tools/helpers'
import { useHistory } from 'react-router-dom'
import {theme} from '../../../tools/theme/'
function Epin() {
    const [code, setCode] = useState('MPTePin')
    const [amount, setAmount] = useState('1000')
    const [remark, setRemark] = useState('')
    const [loading, setLoading] = useState(false)
    const [show, setShow] = useState(false)
    const [clearpin, setClearpin] = useState('')
    const [customerPin, setcustomerPin] = useState('')
    const [expireDate, setExpireDate] = useState('')
    const [step, setStep] = useState(1)
    const history =useHistory()
    const continuePin = () =>{
        let form_data = new FormData()
        form_data.append('billerCode',code)
        form_data.append('rechargeamount',amount)
        form_data.append('remark',remark)
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
                setLoading(true)
                DS_API(b2b_user_data.access_token).post('/mobile/epinenquiry',form_data)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    setShow(true)
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })
            }
            else{
                if(refresh_token(b2b_user_data.refresh_token_old)){
                    setLoading(true)
                    DS_API(localStorage.getItem('access_token')).post('/mobile/epinenquiry',form_data)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                        setShow(true)
                    })
                    .catch(err=>{
                        console.log(err)
                        setLoading(false)
                    })
                }
            }
          }
          else{
             history.push('/') 
          }
    }
    const comfirmEpin = () =>{
        let form_data = new FormData()
        form_data.append('billerCode',code)
        form_data.append('rechargeamount',amount)
        form_data.append('remark',remark)
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
                setLoading(true)
                DS_API(b2b_user_data.access_token).post('/mobile/epinconfirm',form_data)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                   
                    if(res.data.Status.Code==='01'){
                        setClearpin(res.data.Data.Detail.ClearPin)
                        setExpireDate(res.data.Data.Detail.ExpiryDate)  
                        setStep(2)
                        setShow(!show)
                    }
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)

                    setShow(!show)
                })
            }
            else{
                if(refresh_token(b2b_user_data.refresh_token_old)){
                    setLoading(true)
                    DS_API(localStorage.getItem('access_token')).post('/mobile/epinconfirm',form_data)
                    .then(res=>{
                        console.log(res.data)
                        setLoading(false)
                       
                        if(res.data.Status.Code==='01'){
                            setClearpin(res.data.Data.Detail.ClearPin)
                            setExpireDate(res.data.Data.Detail.ExpiryDate)  
                            setStep(2)

                        setShow(!show)
                        }
                    })
                    .catch(err=>{
                        console.log(err)
                        setLoading(false)

                        setShow(!show)
                    })
                }
            }
    }
    }
    return (
        <div style={{marginTop:10,marginBottom:10}}>
          {
              step===1 ?
              <>
              <FormGroup  style={{marginTop:10,marginBottom:10}}>
                   <ControlLabel>Choose Operator</ControlLabel>
                  
                    <RadioGroup inline value={code} onChange={(v)=>{setCode(v)}}>
                    <Radio value='MPTePin'>MPT</Radio>
                             <Radio value='TelenorePin'>Telenor</Radio>
                             <Radio value='OoredooePin'>Ooredoo</Radio>
                             <Radio value='MyTelePin'>MyTel</Radio>
                             <Radio value='MecTelePin'>MecTel</Radio>
                    </RadioGroup>
                  </FormGroup>
                  <FormGroup style={{marginTop:10,marginBottom:10}}>
                   <ControlLabel>Choose Amount</ControlLabel>
                  
                    <RadioGroup   inline value={amount} onChange={(v)=>{setAmount(v)}}>
                    <Radio value='MPTePin'>MPT</Radio>
                             <Radio value='1000'>1000</Radio>
                             <Radio value='3000'>3000</Radio>
                             <Radio value='5000'>5000</Radio>
                             <Radio value='10000'>10000</Radio>
                    </RadioGroup>
                 
               </FormGroup>
               <FormGroup style={{marginTop:10,marginBottom:10}}>
                   <ControlLabel> Remark </ControlLabel>
                   <Input value={remark} onChange={(v)=>{setRemark(v)}} />
               </FormGroup>
               <FormGroup style={{marginTop:10,marginBottom:10}}>
                   <Button loading={loading} disabled={remark===''?true:false}  onClick={()=>{continuePin()}} appearance='primary' color='green'>Continue</Button>
               </FormGroup>
              </>:null
          }
          {
             step ===2 ?
             <>
              <Message title='Success' type='success' description={
                    <>
                   
                    <p> <span>Biller Code:</span> &nbsp; <small>{code} </small> </p>
                    <p> <span>Amount:</span> &nbsp; <small>{amount} MMK </small> </p>
                    <p> <span>Pin:</span> &nbsp; <small>{clearpin}  </small> </p>
                    <p> <span>Expire Date:</span> &nbsp; <small>{expireDate}  </small> </p>
                    </>
                } />
                <div style={{marginBottom:10,marginTop:10}}>
                   
                    <Button onClick={()=>{setStep(1)}} size='sm' style={{marginRight:10}} appearance='subtle'>Back</Button>
                   
                </div>
             </>:null   
           }
            <Modal show={show} style={{fontFamily:theme.default.fontfamily}}> 
                <Modal.Title>EPIN</Modal.Title>
                <Modal.Body>
                    <p> Biller Code - <small style={{color:'tomato'}}>{code} </small> </p>
                    <p> Recharge Amount - <small style={{color:'tomato'}}>{amount} </small> </p>
                    <p> Remark - <small style={{color:'tomato'}}>{remark} </small> </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button appearance='primary' onClick={()=>{setShow(!show)}} color='orange'>Cancel </Button><Button loading={loading} onClick={()=>{comfirmEpin()}} appearance='primary' color='green'>Confirm</Button>
                </Modal.Footer>
           </Modal> 
        </div>
    )
}

export default Epin
