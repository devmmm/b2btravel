import React, { useState, useEffect } from "react";
import { check_token, refresh_token } from "../../tools/helpers/";
import { BE_API } from "../../tools/api";
import LoadPage from "./loader";
import { Switch, Route, Redirect, useHistory } from "react-router-dom";
import DashboardHome from "./dashboard_home";
import MainHeader from "./Navigators/main_header";
import MainSideBar from "./Navigators/main_sidebar";
import B2BBusienss_Entry from "./b2b_busienss_entry";
import B2BBus from "./b2b_bus";
import B2BDigitalService from "./b2b_digital_services/";
import B2BFlight from "./b2b_flight";
function B2BDashboard({ match }) {
  const access_token = localStorage.getItem("access_token");
  const create_time = localStorage.getItem("create_time");
  const refresh_token_old = localStorage.getItem("refresh_token");
  const history = useHistory();
  const [userData, setUserData] = useState(null);
  const [userModules, setUserModules] = useState([]);
  const [Rates, setRates] = useState([]);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (access_token) {
      setLoading(true);
      if (check_token(create_time)) {
        BE_API(access_token)
          .get("/user/profile/")
          .then((res) => {
            console.log(res.data);
            setLoading(false);
            setUserData(res.data.Data);
            setUserModules(res.data.Data.userModule);
          })
          .catch((err) => {
            console.log(err);
            setLoading(false);
          });
      } else {
        if (refresh_token(refresh_token_old)) {
          BE_API(access_token)
            .get("/user/profile/")
            .then((res) => {
              console.log(res.data);
              setUserData(res.data.Data);
              setLoading(false);
              setUserModules(res.data.Data.userModule);
            })
            .catch((err) => {
              console.log(err);
              setLoading(false);
              history.push("/");
            });
        }
      }
    } else {
      history.push("/");
    }
  }, []);

  useEffect(() => {
    if (access_token !== null) {
      if (check_token(create_time)) {
        BE_API(access_token)
          .get("/user/exchangerate/")
          .then((res) => {
            console.log(res.data);
            setRates(res.data.Data.rates);
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        if (refresh_token(refresh_token_old)) {
          BE_API(localStorage.getItem("access_token"))
            .get("/user/exchangerate/")
            .then((res) => {
              console.log(res.data);
              setRates(res.data.Data.rates);
            })
            .catch((error) => {
              console.log(error);
              history.push("/");
            });
        }
      }
    }
  }, []);

  const access_Modules = (module) => {
    if (module === "bus") {
      return <B2BBus userData={userData} route={`${match.path}/${module}`} />;
    } else if (module === "mobile") {
      return (
        <B2BDigitalService
          userData={userData}
          route={`${match.path}/${module}`}
        />
      );
    } else if (module === "flight") {
      return (
        <B2BFlight userData={userData} route={`${match.path}/${module}`} />
      );
    }
  };
  return (
    <div>
      {loading ? (
        <LoadPage />
      ) : (
        <div>
          {userData !== null ? (
            <>
              <MainHeader
                show={show}
                setShow={setShow}
                rates={Rates}
                username={userData.userName}
              />
              <MainSideBar
                show={show}
                setShow={setShow}
                userModules={userModules}
              />
              <Switch>
                <Route exact path={`${match.path}`} component={DashboardHome} />
                <Route
                  path={`${match.path}/business_entry`}
                  component={() => {
                    return (
                      <B2BBusienss_Entry
                        route={`${match.path}/business_entry`}
                        userData={userData}
                      />
                    );
                  }}
                />
                {userModules.map((um, i) => (
                  <Route
                    key={i}
                    path={`${match.path}/${um.moduleURL}`}
                    component={() => access_Modules(um.moduleURL)}
                  />
                ))}
              </Switch>
            </>
          ) : null}
        </div>
      )}
    </div>
  );
}

export default B2BDashboard;
