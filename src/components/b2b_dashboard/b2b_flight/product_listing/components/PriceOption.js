import React from 'react'

function PriceOption(props) {
    const data = props.data;
    const option = props.priceoption
    return (
        <div className='flight_lising_pricing'>
            <div className='price_tags'>
              {data.totalFare} {data.currency}
            </div>
            <div className='option'>
               {
                   option ? option.map((o,i)=>(
                       <span key={i}>{o} </span>
                   )):null
               }
            </div>
           
        </div>
    )
}

export default PriceOption
