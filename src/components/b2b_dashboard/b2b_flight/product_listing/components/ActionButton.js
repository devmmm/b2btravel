import React, { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'

import { Button, Col, Divider, Modal, Notification, Row } from 'rsuite'
import { FLIGHT_API } from '../../../../../tools/api';
import { b2b_user_data, b2b_variables } from '../../../../../tools/constants';
import { check_token, refresh_token } from '../../../../../tools/helpers';

function ActionButton(props) {
const {triptype,deptfrom,goingto,depdate,classes,adult,child,infant} = useParams()
const searchID= props.searchID;
const fareSourceCode = props.fareSourceCode;
const fareSource=props.fareSource;
const [loading, setLoading] = useState(false)
const [rules, setRules] = useState(null)
const [show, setShow] = useState(false)
const history = useHistory()
const requestRule = () =>{
    var FormData = require('form-data');
var data = new FormData();
data.append('fsc', fareSourceCode);
data.append('src', fareSource);
setLoading(true)
 if(b2b_user_data.access_token!==null){
     if(check_token(b2b_user_data.create_time)){
         FLIGHT_API(b2b_user_data.access_token).post('/farerules',data)
         .then(res=>{
             console.log(res.data)
             setLoading(false)
             if(res.data.Data!==null){
                setShow(true)
                setRules(res.data.Data)
             }
             else{
                Notification.warning({
                    title:'b2b.travels.com.mm',
                    description:'Data not avaliable!',
                    style:{
                        fontFamily:b2b_variables.system_font
                    }
                })  
             }
             
         })
         .catch(err=>{
             console.log(err)
             Notification.warning({
                title:'b2b.travels.com.mm',
                description:'Something Wrong!',
                style:{
                    fontFamily:b2b_variables.system_font
                }
            })
         })
     }
     else{
         if(refresh_token(b2b_user_data.refresh_token_old)){
            FLIGHT_API(b2b_user_data.access_token).post('/farerules',data)
            .then(res=>{
                console.log(res.data)
                setLoading(false)
                setShow(true)
                
            })
            .catch(err=>{
                console.log(err)
                Notification.warning({
                    title:'b2b.travels.com.mm',
                    description:'Something Wrong!',
                    style:{
                        fontFamily:b2b_variables.system_font
                    }
                })
            })
         }
     }
 }
}
const chooseFlight = () =>{
 history.push(`/dashboard/flight/booking/${triptype}/${deptfrom}/${goingto}/${classes}/${adult}/${child}/${infant}/${depdate}/${fareSourceCode}/${fareSource}/${searchID}`)
}
    return (
        <div className='flight_select_action_button'>
             <div className='farerule'>
            <Button loading={loading} onClick={()=>{requestRule()}} appearance='ghost' color='yellow' size='sm'>Fare Rules</Button>
           </div>
           <div className='select_button'>
           <Button onClick={()=>{chooseFlight()}} appearance='primary' color='green' size='md' >Select</Button>
           </div>
          <Modal  style={{fontFamily:b2b_variables.system_font}} show={show} onHide={()=>{setShow(!show)}}>
              <Modal.Title> FareRules </Modal.Title>
               <Modal.Body>
                 {
                     rules!==null ? 
                     <div className='flight_rules'>
                        {
                            rules.fareRules ? rules.fareRules.map((rule,i)=>(
                            <div className='farerules' key={i}>
                                 <div className='heading_div'>
                                 <p className='heading'> Airline - {rule.Airline} | {rule.CityPair} </p>
                                 </div>
                                 {
                                     rule.RuleDetails.map((d,i)=>(
                                        <div className='body' key={i}>
                                        <Divider className='category'>
                                             {d.Category}
                                        </Divider>
                                        <div className='text'>
                                           - {d.Rules}
                                        </div>
                                     </div>
                                     ))
                                 }
                                 
                            </div>
                            )):null
                            
                        }
                         <div className='baginfo'  > 
                                  <Divider className='title'>BaggageInfos</Divider>
                                  <Row>
                                               <Col xs={6} sm={6} md={6} lg={6}>
                                                 <p className='heading'>Arrival </p>
                                               </Col>
                                               <Col xs={6} sm={6} md={6} lg={6}>
                                               <p className='heading'>Baggage </p>
                                                </Col>
                                                <Col xs={6} sm={6} md={6} lg={6}>
                                                <p className='heading'>Departure </p>
                                               </Col>
                                               <Col xs={6} sm={6} md={6} lg={6}>
                                               <p className='heading'>FlightNo </p>
                                               </Col>
                                           </Row>
                                 {
                                   rules.BaggageInfos?  rules.BaggageInfos.map((b,i)=>(
                                        
                                           <Row key={i}>
                                               <Col xs={6} sm={6} md={6} lg={6}>
                                                 <p className='text'>{b.Arrival} </p>
                                               </Col>
                                               <Col xs={6} sm={6} md={6} lg={6}>
                                               <p className='text'>{b.Baggage} </p>
                                                </Col>
                                                <Col xs={6} sm={6} md={6} lg={6}>
                                                <p className='text'>{b.Departure} </p>
                                               </Col>
                                               <Col xs={6} sm={6} md={6} lg={6}>
                                               <p className='text'>{b.FlightNo} </p>
                                               </Col>
                                           </Row>
                                       
                                     )):null
                                 }
                                   </div>
                     </div>:null
                 }
               </Modal.Body>
               <Modal.Footer>
                   <Button appearance='primary' color='red' size='md' onClick={()=>{setShow(!show)}} >Close</Button>
               </Modal.Footer>
          </Modal>  
        </div>
    )
}

export default ActionButton
