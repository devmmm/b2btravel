import React from 'react'
import { Col, Icon, Row } from 'rsuite';

function Segments(props) {
    const data = props.data;
    return (
        <div className='flight_product_panel'>
            {
                data ? data.map((d,i)=>(
                    <div className='segments' key={i}>
                      {
                          d.segments.map((s,i)=>(
                              <div className='depname' key={i}>
                                  <Row>
                                      <Col xs={8} sm={8} md={8} lg={8}>
                                      {s.departureAirportCode} {i!==s.length-1 ? <Icon icon='plane' />:null} {s.arrivalAirportCode}
                                      </Col>
                                      <Col xs={8} sm={8} md={8} lg={8}>
                                      <span className='time'> &nbsp; <Icon icon='clock-o' /> {s.journeyDuration} min</span>
                                      </Col>
                                      <Col xs={8} sm={8} md={8} lg={8}>
                                      <span className='stop'> &nbsp; <Icon icon='stop-circle-o' /> {s.stopQuantity} stop</span>
                                      </Col>
                                  </Row>
                              
                              
                              </div>
                          ))
                      }
                      {
                          d.segments.map((s,i)=>(
                              <div className='deptime' key={i}>
                               {i===0 ?  <div className='time'><Icon icon='calendar' /> Depture -  {new Date(s.departureDateTime).toLocaleString()}</div>:null} {i===d.segments.length-1 ? <div div className='time'> <Icon icon='calendar' /> Arrival -  {new Date(s.arrivalDateTime).toLocaleString()} </div>:null}
                               
                              </div>
                          ))
                      }
                    </div>
                )):null
          }
        </div>
    )
}

export default Segments
