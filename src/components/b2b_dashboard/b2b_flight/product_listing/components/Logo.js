import React from 'react'
import './assets/sprite.css'
import LOGO from './assets/airline.png'
import { Col, Row } from 'rsuite'
function Logo(props) {
    const data = props.data
    return (
        <div className='flight_product_panel'>
          {
                data ? data.map((d,i)=>(
                    <div className='segments' key={i}>
                      {
                          d.segments.map((s,i)=>(
                              <div className='logo_text' key={i}>
                                <Row>
                                    <Col xs={12} sm={12} md={12} lg={12}>
                                     {
                                         i===0 ?    <div style={{backgroundImage: "url(" +LOGO + ")",borderRadius:'50%'}} className={"airline-"+s.operatingCarrierCode}>
                                         </div>:null
                                     }
                                    </Col>
                                    <Col xs={12} sm={12} md={12} lg={12}>
                                     {
                                         i===0?    <p className='text'> {s.carrierName}</p>:null
                                     }
                                     </Col>
                                </Row>
                               
                               
                              </div>
                          ))
                      }
                    </div>
                )):null
          }
        </div>
    )
}

export default Logo
