import React from 'react'
import { Button, Col, Icon, Message, Panel, Row } from 'rsuite'
import ActionButton from './components/ActionButton'
import Logo from './components/Logo'
import PriceOption from './components/PriceOption'
import Segments from './components/Segments'

function Products(props) {
    
    return (
        <div>
           {
               props.data ? props.data.map((d,i)=>(
                   <Panel className='flight_product_list' key={i} header={<p className='flight_products_heading'> {d.searchID + "|" + d.fareSource } | <Icon icon='plane' /> {d.tripType} </p>} >
                     <Row>
                         <Col xs={24} sm={24} md={4} lg={4}>
                             <Logo data={d.itineraries} />
                         </Col>
                         <Col xs={24} sm={24} md={12} lg={12}>
                             <Segments data={d.itineraries} />
                         </Col>
                         <Col xs={24} sm={24} md={4} lg={4}>
                             <PriceOption data={d.pricing} priceoption={d.pricingOptions}  />
                         </Col>
                         <Col xs={24} sm={24} md={4} lg={4}>
                            <ActionButton searchID={d.searchID} fareSourceCode={d.fareSourceCode} fareSource={d.fareSource} params={props.params} />
                         </Col>
                     </Row>
                   </Panel>
               )):null
           }
           {
               props.data.length===0 ? 
               <div>
                   <Message title='b2b.travels.com.mm' type='warning' showIcon description={
                       <div className='message_div'>
                          
                         <div className='body_text'>
                             Not Avaliable Now!
                         </div>
                         <div className='message_action_button'>
                             <Button onClick={()=>{window.location.reload()}} size='sm' appearance='ghost' color='yellow'>Try Again</Button>
                         </div>
                        </div>
                   } />
                   </div>:null
           }
           


        </div>
    )
}

export default Products
