import React, { useEffect, useState } from 'react'

import { Divider, Icon, Grid, Row, Col } from 'rsuite'
import { check_token, formatDate, refresh_token } from '../../../../tools/helpers'
import InlineSearch from '../searchbox/InlineSearch'
import Products from './Products'
import { b2b_user_data } from '../../../../tools/constants'
import { FLIGHT_API } from '../../../../tools/api'
import FlightSample from '../../../../assets/sampleFlightData.json'
import { useParams } from 'react-router-dom'
import ProductLoader from './components/ProductLoader'
function ProductListing(props) {
    const depfrom = props.match.params.deptfrom
    const goingto= props.match.params.goingto
    const triptype = props.match.params.triptype
    const classes= props.match.params.classes
    const adult = props.match.params.adult
    const child = props.match.params.child
    const infant = props.match.params.infant
    const params = useParams()
    const selectDate = props.match.params.triptype==='oneway'?new Date(props.match.params.depdate):[new Date(props.match.params.depdate.substr(0, 10)), new Date(props.match.params.depdate.substr(11, 10))]
    const [loading, setLoading] = useState(false)
    const [flightData, setflightData] = useState([])
    useEffect(() => {
       setLoading(true)
      if(b2b_user_data.access_token!==null){
        if(check_token(b2b_user_data.create_time)){
         if(triptype==='roundtrip'){
          FLIGHT_API(b2b_user_data.access_token).get(`/intflightsearch?tripType=${triptype}&origin=${depfrom}&destination=${goingto}&departureDate=${props.match.params.depdate.substr(0, 10)}&returnDate=${props.match.params.depdate.substr(11, 10)}&excludedAirlines=&includeAirlines=&nonStop=false&adults=${adult}&children=${child}&infants=${infant}&travelClass=${classes}&currencyCode=USD`)
          .then(res=>{
            console.log(res.data)
            setLoading(false)
            setflightData(res.data.Data)
          })
          .catch(err=>{
            setLoading(false)
            console.log(err)
          })
         }
         else{
          FLIGHT_API(b2b_user_data.access_token).get(`/intflightsearch?tripType=${triptype}&origin=${depfrom}&destination=${goingto}&departureDate=${props.match.params.depdate}&excludedAirlines=&includeAirlines=&nonStop=false&adults=${adult}&children=${child}&infants=${infant}&travelClass=${classes}&currencyCode=USD`)
          .then(res=>{
            console.log(res.data)
            setLoading(false)
            setflightData(res.data.Data)
          })
          .catch(err=>{
            setLoading(false)
            console.log(err)
          })
         }
        }
        else{
         refresh_token(b2b_user_data.refresh_token_old)
         if(triptype==='roundtrip'){
          FLIGHT_API(b2b_user_data.access_token).get(`/intflightsearch?tripType=${triptype}&origin=${depfrom}&destination=${goingto}&departureDate=${props.match.params.depdate}&excludedAirlines=&includeAirlines=&nonStop=false&adults=${adult}&children=${child}&infants=${infant}&travelClass=${classes}&currencyCode=USD`)
          .then(res=>{
            console.log(res.data)
            setflightData(res.data.Data)
            setLoading(false)
          })
          .catch(err=>{
            setLoading(false)
            console.log(err)
          })
         }
         else{
          FLIGHT_API(b2b_user_data.access_token).get(`/intflightsearch?tripType=${triptype}&origin=${depfrom}&destination=${goingto}&departureDate=${props.match.params.depdate}&excludedAirlines=&includeAirlines=&nonStop=false&adults=${adult}&children=${child}&infants=${infant}&travelClass=${classes}&currencyCode=USD`)
          .then(res=>{
            console.log(res.data)
            setflightData(res.data.Data)
            setLoading(false)
          })
          .catch(err=>{
            setLoading(false)
            console.log(err)
          })
         }
        }

      }
    }, [depfrom,goingto,triptype,classes,adult,child,infant])
    return (
        <div>
        <Divider style={{textAlign:'center'}}> <p style={{color:'brown'}}> {depfrom} <Icon icon='plane' /> {goingto}</p> 
         <small style={{color:'grey'}}> <Icon icon='calendar' /> {props.match.params.depdate} <Divider vertical /> <Icon icon='plane' /> {triptype} <Divider vertical /> <Icon icon='peoples' /> {parseInt(adult)+parseInt(child) +parseInt(infant)} </small>
         </Divider>
        <Grid>
            <Row>
                <Col xs={24} sm={24} md={8} lg={8}>
                  <div className='inline_flight_search_form'>
                   <InlineSearch depfrom={depfrom}  goingto={goingto}  triptype={triptype}  classes={classes}  adult={adult}  child={child} infant={infant} depdate={props.match.params.depdate}  />
                  </div>
                </Col>
                <Col xs={24} sm={24} md={16} lg={16}>
                  <div className='flight_product_listing'>
                   {
                     loading ? 
                     <ProductLoader />:
                     <Products  data={flightData}  />
                   }
                  </div>
                </Col>
            </Row>
        </Grid>
        </div>
    )
}

export default ProductListing
