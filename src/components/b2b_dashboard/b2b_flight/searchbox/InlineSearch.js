import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Panel, Row, Col, FormGroup, ControlLabel, RadioGroup, Radio, Alert,SelectPicker,Icon,Popover,InputGroup,InputNumber,Whisper,Button,Divider,DatePicker,DateRangePicker } from 'rsuite'
import { FLIGHT_API } from '../../../../tools/api'
import { b2b_user_data, b2b_variables } from '../../../../tools/constants'
import { check_token, formatDate, refresh_token } from '../../../../tools/helpers'

function InlineSearch(props) {
  const [depfrom, setDepfrom] = useState(props.depfrom)
  const [goingto, setGoingto] = useState(props.goingto)
  const [triptype, setTriptype] = useState(props.triptype)
  const [classes, setClasses] = useState(props.classes)
  const [child, setChild] = useState(props.child)
  const [adult, setAdult] = useState(props.adult)
  const [infant, setInfant] = useState(props.infant)
  const [depdate, setDepdate] = useState( props.triptype==='oneway'?new Date(props.depdate):[new Date(props.depdate.substr(0, 10)), new Date(props.depdate.substr(11, 10))])
  const [airportfrom, setAirportfrom] = useState([])
    const [airportto, setAirportto] = useState([])
    const [loading, setLoading] = useState(false)
    const history = useHistory()
  const handleSearchFrom = (search_text) =>{
    if(search_text.length>1){
        setLoading(true)
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
            FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
            .then(res=>{
               setLoading(false)
               setAirportfrom(res.data.Data)

            })
            .catch(err=>{
                Alert.warning(JSON.stringify(err))
                setLoading(false)
            })
            }
            else{

                refresh_token(b2b_user_data.refresh_token_old)
                FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
                .then(res=>{
                   setLoading(false)
                   setAirportfrom(res.data.Data)
                })
                .catch(err=>{
                    Alert.warning(JSON.stringify(err))
                    setLoading(false)
                })
            }
        }
    }
}

// end Searchfrom

// SearchTo
const handleSearchTo = (search_text) =>{
    if(search_text.length>1){
        setLoading(true)
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
            FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
            .then(res=>{
               setLoading(false)
               setAirportto(res.data.Data)

            })
            .catch(err=>{
                Alert.warning(JSON.stringify(err))
                setLoading(false)
            })
            }
            else{

                refresh_token(b2b_user_data.refresh_token_old)
                FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
                .then(res=>{
                   setLoading(false)
                   setAirportto(res.data.Data)
                })
                .catch(err=>{
                    Alert.warning(JSON.stringify(err))
                    setLoading(false)
                })
            }
        }
    }
}

// end of SearchTo
const InputTraveller = ({...props})=>{
  return(
      <Popover title='Travellers' {...props} >
        <div className='travellers'>
          <InputGroup style={{marginBottom:10}}>
              <InputGroup.Addon>
               <ControlLabel>Adult</ControlLabel>
              </InputGroup.Addon>
              <InputNumber min={1} value={adult} onChange={(v)=>{setAdult(v)}} />
          </InputGroup>
          <InputGroup style={{marginBottom:10}}>
              <InputGroup.Addon>
               <ControlLabel>Child</ControlLabel>
              </InputGroup.Addon>
              <InputNumber min={0} value={child} onChange={(v)=>{setChild(v)}} />
          </InputGroup>
          <InputGroup style={{marginBottom:10}}>
              <InputGroup.Addon>
               <ControlLabel>Infant</ControlLabel>
              </InputGroup.Addon>
              <InputNumber min={0} value={infant} onChange={(v)=>{setInfant(v)}} />
          </InputGroup>
          <RadioGroup value={classes} onChange={(v)=>{setClasses(v)}} >
              <Radio value='Y'>Economy</Radio>
              <Radio value='C'>Business</Radio>
              <Radio value='F'>First</Radio>
              <Radio value='S'>Premium Economy</Radio>
          </RadioGroup>
        </div>
      </Popover>
  )
}

const GoSearch = () =>{
  if(depfrom===goingto){
      Notification.warning({
          title:'b2b.travels.com.mm',
          description:'Where do you want to go?',
          duration:10000,
          style:{
              fontFamily:b2b_variables.system_font,
          }
      })
  }
  else{
      if(triptype==='oneway'){
          var depdatevar = formatDate(depdate)
          history.push(`/dashboard/flight/flight_search/${triptype}/${depfrom}/${goingto}/${depdatevar}/${classes}/${adult}/${child}/${infant}`)
       }
       else{
           var deptdatevar = formatDate(depdate[0])
           var arrdate = formatDate(depdate[1])
           history.push(`/dashboard/flight/flight_search/${triptype}/${depfrom}/${goingto}/${depdatevar}_${arrdate}/${classes}/${adult}/${child}/${infant}`)
       }
  }
}

    return (
        <div className='flight_inline_search'>
            <Panel style={{background:'#f2f2f2'}}>
                <Row>
                  <Col xs={24} sm={24} md={24} lg={24} >
                    <FormGroup style={{marginBottom:10}}>
                       <ControlLabel>
                         TripType:   
                       </ControlLabel> &nbsp;
                       <RadioGroup value={triptype} onChange={(v)=>{setTriptype(v)}} inline appearance='picker' >
                       <Radio value='oneway'>OneWay</Radio>
                        <Radio value='roundtrip'>RoundTrip</Radio>
                       </RadioGroup>
                    </FormGroup>
                    <FormGroup>
                    <ControlLabel>Pressengers :</ControlLabel>
                                 <Whisper trigger='click' placement='bottom' speaker={<InputTraveller />} >
                                  <Button appearance='subtle'>
                                   {adult} Adult  <Divider vertical /> {child} Child  <Divider vertical /> {infant} Infant  <Divider vertical /> {classes}
                                  </Button>
                                 </Whisper>
                    </FormGroup>
                    <FormGroup>
                               <ControlLabel><Icon icon='map-marker' /> Leaving From </ControlLabel>
                               <SelectPicker
                               block
                                data={airportfrom}
                                valueKey='iata_code'
                                labelKey='name'
                                value={depfrom}
                                onChange={(v)=>{setDepfrom(v)}}
                                onSearch={(search_text)=>{handleSearchFrom(search_text)}}
                                renderMenu={
                                    menu=>{
                                        if(loading){
                                            return(
                                                <p style={{padding:4,color:'green',textAlign:'center'}}>
                                                    <Icon icon='spinner' spin />
                                                </p>
                                            )

                                        }
                                        return menu;
                                    }
                                }
                               />
                               </FormGroup>
                               <FormGroup>
                               <ControlLabel><Icon icon='map-marker' /> Going To </ControlLabel>
                               <SelectPicker
                               block
                                data={airportto}
                                valueKey='iata_code'
                                labelKey='name'
                                value={goingto}
                                onChange={(v)=>{setGoingto(v)}}
                                onSearch={(search_text)=>{handleSearchTo(search_text)}}
                                renderMenu={
                                    menu=>{
                                        if(loading){
                                            return(
                                                <p style={{padding:4,color:'green',textAlign:'center'}}>
                                                    <Icon icon='spinner' spin />
                                                </p>
                                            )

                                        }
                                        return menu;
                                    }
                                }
                               />
                               </FormGroup>
                               <FormGroup>
                                 <ControlLabel><Icon icon='calendar' /> Departure Date:</ControlLabel>
                               {
                                 triptype==='oneway'?
                                 <DatePicker block value={depdate} onChange={(v)=>{setDepdate(v)}} />:
                                 <DateRangePicker block value={depdate} onChange={(v)=>{setDepdate(v)}} />
                             }
                               </FormGroup>
                               <FormGroup>
                               <div className='search_button'>
                             <Button onClick={()=>{GoSearch()}} disabled={depfrom==='' || goingto==='' || depdate===''?true:false} appearance='primary' block color='green'>Search</Button>
                             </div>
                               </FormGroup>
                  </Col>
                </Row>
            </Panel>
         
        </div>
    )
}

export default InlineSearch
