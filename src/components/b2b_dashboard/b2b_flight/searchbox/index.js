import React, { useState } from 'react'
import { Panel, Divider, Grid, Row, Col, RadioGroup, Radio, ControlLabel, Popover, InputGroup, InputNumber, Whisper, Button, Alert, Icon, SelectPicker, FormGroup, DatePicker, DateRangePicker, Notification } from 'rsuite'
import { useHistory } from 'react-router-dom'
import { b2b_user_data, b2b_variables } from '../../../../tools/constants'
import { check_token, refresh_token, formatDate } from '../../../../tools/helpers'
import { FLIGHT_API } from '../../../../tools/api'

function SearchBox() {
    const [triptype, settriptype] = useState('oneway')
    const [adult, setAdult] = useState(1)
    const [child, setChild] = useState(0)
    const [infant, setInfant] = useState(0)
    const [classes, setClasses] = useState('Y')
    const [deptfrom, setDeptfrom] = useState('')
    const [goingto, setGoingto] = useState('')
    const [airportfrom, setAirportfrom] = useState([])
    const [airportto, setAirportto] = useState([])
    const [selectDate, setSelectDate] = useState()
    const [loading, setLoading] = useState(false)
    const history = useHistory()
    
    // searchFrom
    const handleSearchFrom = (search_text) =>{
        if(search_text.length>1){
            setLoading(true)
            if(b2b_user_data.access_token!==null){
                if(check_token(b2b_user_data.create_time)){
                FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
                .then(res=>{
                   setLoading(false)
                   setAirportfrom(res.data.Data)

                })
                .catch(err=>{
                    Alert.warning(JSON.stringify(err))
                    setLoading(false)
                })
                }
                else{

                    refresh_token(b2b_user_data.refresh_token_old)
                    FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
                    .then(res=>{
                       setLoading(false)
                       setAirportfrom(res.data.Data)
                    })
                    .catch(err=>{
                        Alert.warning(JSON.stringify(err))
                        setLoading(false)
                    })
                }
            }
        }
    }

    // end Searchfrom

    // SearchTo
    const handleSearchTo = (search_text) =>{
        if(search_text.length>1){
            setLoading(true)
            if(b2b_user_data.access_token!==null){
                if(check_token(b2b_user_data.create_time)){
                FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
                .then(res=>{
                   setLoading(false)
                   setAirportto(res.data.Data)

                })
                .catch(err=>{
                    Alert.warning(JSON.stringify(err))
                    setLoading(false)
                })
                }
                else{

                    refresh_token(b2b_user_data.refresh_token_old)
                    FLIGHT_API(b2b_user_data.access_token).get(`/airportsearch?q=${search_text}`)
                    .then(res=>{
                       setLoading(false)
                       setAirportto(res.data.Data)
                    })
                    .catch(err=>{
                        Alert.warning(JSON.stringify(err))
                        setLoading(false)
                    })
                }
            }
        }
    }

    // end of SearchTo

    const InputTraveller = ({...props})=>{
        return(
            <Popover title='Travellers' {...props} >
              <div className='travellers'>
                <InputGroup style={{marginBottom:10}}>
                    <InputGroup.Addon>
                     <ControlLabel>Adult</ControlLabel>
                    </InputGroup.Addon>
                    <InputNumber min={1} value={adult} onChange={(v)=>{setAdult(v)}} />
                </InputGroup>
                <InputGroup style={{marginBottom:10}}>
                    <InputGroup.Addon>
                     <ControlLabel>Child</ControlLabel>
                    </InputGroup.Addon>
                    <InputNumber min={0} value={child} onChange={(v)=>{setChild(v)}} />
                </InputGroup>
                <InputGroup style={{marginBottom:10}}>
                    <InputGroup.Addon>
                     <ControlLabel>Infant</ControlLabel>
                    </InputGroup.Addon>
                    <InputNumber min={0} value={infant} onChange={(v)=>{setInfant(v)}} />
                </InputGroup>
                <RadioGroup value={classes} onChange={(v)=>{setClasses(v)}} >
                    <Radio value='Y'>Economy</Radio>
                    <Radio value='C'>Business</Radio>
                    <Radio value='F'>First</Radio>
                    <Radio value='S'>Premium Economy</Radio>
                </RadioGroup>
              </div>
            </Popover>
        )
    }

    const GoSearch = () =>{
        if(deptfrom===goingto){
            Notification.warning({
                title:'b2b.travels.com.mm',
                description:'Where do you want to go?',
                duration:10000,
                style:{
                    fontFamily:b2b_variables.system_font,
                }
            })
        }
        else{
            if(triptype==='oneway'){
                var depdate = formatDate(selectDate)
                history.push(`/dashboard/flight/flight_search/${triptype}/${deptfrom}/${goingto}/${depdate}/${classes}/${adult}/${child}/${infant}`)
             }
             else{
                 var deptdate = formatDate(selectDate[0])
                 var arrdate = formatDate(selectDate[1])
                 history.push(`/dashboard/flight/flight_search/${triptype}/${deptfrom}/${goingto}/${deptdate}_${arrdate}/${classes}/${adult}/${child}/${infant}`)
             }
        }
    }
    return (
        <div>
            <Divider style={{fontWeight:'bold',textTransform:'uppercase'}}>Flight Search </Divider>
          <Grid>
              <Row>
                  <Col xs={24} sm={24} md={24} lg={24} >
                      <Panel style={{background:'#f2f2f2'}}>
                        <Row>
                            <Col xs={24} sm={24} md={12} lg={12}>
                              <div className='flight_form'>
                                  <ControlLabel>Select Trip Type:</ControlLabel>
                                  <RadioGroup value={triptype} inline appearance='picker' onChange={(v)=>{settriptype(v)}} >
                                    <Radio value='oneway' >OneWay</Radio>
                                    <Radio value='roundtrip' >RoundTrip</Radio>
                                  </RadioGroup>
                              </div>
                            </Col>
                            <Col xs={24} sm={24} md={12} lg={12}>
                               <div className='flight_form'>
                                 <ControlLabel>Pressengers :</ControlLabel>
                                 <Whisper trigger='click' placement='bottom' speaker={<InputTraveller />} >
                                  <Button appearance='subtle'>
                                   {adult} Adult  <Divider vertical /> {child} Child  <Divider vertical /> {infant} Infant  <Divider vertical /> {classes}
                                  </Button>
                                 </Whisper>
                               </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={24} sm={24} md={6} lg={6}>
                              <div className='flight_form'>
                               <FormGroup>
                               <ControlLabel><Icon icon='map-marker' /> Leaving From </ControlLabel>
                               <SelectPicker
                               block
                                data={airportfrom}
                                valueKey='iata_code'
                                labelKey='name'
                                value={deptfrom}
                                onChange={(v)=>{setDeptfrom(v)}}
                                onSearch={(search_text)=>{handleSearchFrom(search_text)}}
                                renderMenu={
                                    menu=>{
                                        if(loading){
                                            return(
                                                <p style={{padding:4,color:'green',textAlign:'center'}}>
                                                    <Icon icon='spinner' spin />
                                                </p>
                                            )

                                        }
                                        return menu;
                                    }
                                }
                               />
                               </FormGroup>
                                
                              </div>
                            </Col>
                            <Col xs={24} sm={24} md={6} lg={6}>
                            <div className='flight_form'>
                            <FormGroup>
                               <ControlLabel><Icon icon='map-marker' /> Going To </ControlLabel>
                               <SelectPicker
                               block
                                data={airportto}
                                valueKey='iata_code'
                                labelKey='name'
                                value={goingto}
                                onChange={(v)=>{setGoingto(v)}}
                                onSearch={(search_text)=>{handleSearchTo(search_text)}}
                                renderMenu={
                                    menu=>{
                                        if(loading){
                                            return(
                                                <p style={{padding:4,color:'green',textAlign:'center'}}>
                                                    <Icon icon='spinner' spin />
                                                </p>
                                            )

                                        }
                                        return menu;
                                    }
                                }
                               />
                               </FormGroup>
                            </div>
                            </Col>
                            <Col xs={24} sm={24} md={8} lg={8}>
                            <div className='flight_form'>
                             <ControlLabel>Choose Date</ControlLabel>
                             {
                                 triptype==='oneway'?
                                 <DatePicker block value={selectDate} onChange={(v)=>{setSelectDate(v)}} />:
                                 <DateRangePicker block value={selectDate} onChange={(v)=>{setSelectDate(v)}} />
                             }
                            </div>
                            </Col>
                            <Col xs={24} sm={24} md={4} lg={4}>
                            <div className='flight_form'>
                             <div className='search_button'>
                             <Button onClick={()=>{GoSearch()}} disabled={deptfrom==='' || goingto==='' || selectDate===''?true:false} appearance='primary' block color='green'>Search</Button>
                             </div>
                            </div>
                            </Col>
                        </Row>
                      </Panel>
                  </Col>
              </Row>
          </Grid>
        </div>
    )
}

export default SearchBox
