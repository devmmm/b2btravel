import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Divider,Grid,Col,Button, Icon, Row, Panel } from 'rsuite'
import { FLIGHT_API } from '../../../../tools/api'
import { b2b_user_data } from '../../../../tools/constants'
import { check_token, refresh_token } from '../../../../tools/helpers'

function FlightOfflineBookingList() {
    const [bookings, setBookings] = useState([])
    const [loading, setLoading] = useState(false)
    const history = useHistory()
    
    useEffect(() => {
      setLoading(true)
        if(b2b_user_data.access_token!==null){
           
         if(check_token(b2b_user_data.create_time)){
             FLIGHT_API(b2b_user_data.access_token).get('/getofflinebooking')
             .then(res=>{
                 setLoading(false)
                 setBookings(res.data.Data)
             })
             .catch(err=>{
                 setLoading(false)
                 console.log(err)
             })
         }
         else{
            if(refresh_token(b2b_user_data.refresh_token_old)) {
              FLIGHT_API(b2b_user_data.access_token).get('/getofflinebooking')
              .then(res=>{
                  setLoading(false)
                  setBookings(res.data.Data)
              })
              .catch(err=>{
                  setLoading(false)
                  console.log(err)
              })
            }
            
         }
        }
    
    }, [])
    return (
        <div>
            <Divider style={{fontWeight:'bold',fontSize:'18px',textTransform:'uppercase'}}> Flight Offline Order Booking List </Divider>
        
            <Grid >
             <Row>
               {
                 loading ?
                 <>
                 <div className='bus_booking_detial_loader'>
                     <Icon spin icon='spinner' size='2x' />
                  </div>
                 </>
                 :
                 <Col sm={24} xs={24} md={24} lg={24} >
                 {
                   bookings.map((b,i)=>(
                     <Panel key={i} className='flight_booking_list' header={<p className='heading'>{b.pnr} </p>}>
                       <Row>
                         <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='label'> <Icon icon='plane' /> Airline </p>
                         </Col>
                         <Col xs={12} sm={12} md={12} lg={12}>
                         <p className='value'>  {b.airline} </p>
                         </Col>
                       </Row>
                       <Divider />
                       <Row>
                         <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='label'> <Icon icon='user' /> TravelerName </p>
                         </Col>
                         <Col xs={12} sm={12} md={12} lg={12}>
                         <p className='value'>  {b.pax_name} </p>
                         </Col>
                       </Row>
                       <Row>
                         <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='label'> <Icon icon='peoples' /> No. of Travelers </p>
                         </Col>
                         <Col xs={12} sm={12} md={12} lg={12}>
                         <p className='value'>  {b.pax_no} </p>
                         </Col>
                       </Row>
                       <Row>
                         <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='label'> <Icon icon='calendar' />Booking Date </p>
                         </Col>
                         <Col xs={12} sm={12} md={12} lg={12}>
                         <p className='value'>  {b.bookDate} </p>
                         </Col>
                       </Row>
                       <Divider />
                       <Row>
                         <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='label'> <Icon icon='info' />Status </p>
                         </Col>
                         <Col xs={12} sm={12} md={12} lg={12}>
                         <p className={b.bookingStatus==='PENDING'?'status_pending':'status_success'}>  {b.bookingStatus} </p>
                         </Col>
                         <Col xs={12} sm={12} md={12} lg={12}>
                          <Button onClick={()=>{history.push(`/dashboard/flight/booking/flight_booking_detial/${b.id}`)}} appearance='primary' color='green' size='md' className='flight_d_action_button' >Detial</Button>
                         </Col>
                       </Row>
                     </Panel>
                   ))
                 }
                </Col>
               }
             </Row>
          </Grid>
        
        </div>
    )
}

export default FlightOfflineBookingList
