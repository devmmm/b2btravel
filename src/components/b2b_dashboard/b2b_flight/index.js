import React from 'react'
import { Switch, Route, Redirect, useRouteMatch } from 'react-router-dom'
import FlightHome from './home'
import SearchBox from './searchbox'
import ProductListing from './product_listing'
import OrderOfflineBooking from './orderofflinebooking'
import FlightOfflineBookingList from './flight_offline_order_booking_list'
import OfflineFlightBookingDetail from './orderofflinebooking/detial'
import FlightBooking from './FlightBooking'

function B2BFlight(props) {
   const {path} = useRouteMatch()
    return (
        <div>
            <Switch>
                <Route exact path={path} component={FlightHome} />
                <Route exact path={`${path}/flight_search`} component={SearchBox} />
                <Route exact path={`${path}/flight_search/:triptype/:deptfrom/:goingto/:depdate/:classes/:adult/:child/:infant`} component={ProductListing} />
                <Route exact path={`${path}/flight_order_offline_booking`} component={OrderOfflineBooking} />
                <Route exact path={`${path}/flight_offline_bookings_list`} component={FlightOfflineBookingList} />
                <Route exact path={`${path}/booking/flight_booking_detial/:id`} component={OfflineFlightBookingDetail} />
                <Route exact  path={`${path}/booking/:triptype/:depfrom/:goingto/:fclass/:adult/:child/:infant/:depdate/:code/:fc/:sid`} component={()=>{return <FlightBooking  userData={props.userData} />} } />
                <Redirect to='/404' />
            </Switch>
           
        </div>
    )
}

export default B2BFlight
