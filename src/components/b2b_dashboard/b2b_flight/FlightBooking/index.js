import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Button, Col, Divider, Grid, Icon, Modal, Row, Steps } from 'rsuite'
import { FLIGHT_API } from '../../../../tools/api'
import { b2b_user_data, b2b_variables } from '../../../../tools/constants'
import { check_token, refresh_token } from '../../../../tools/helpers'
import BookingLoader from './BookingLoading'
import YourFlight from './YourFlight'
import {flight_info} from '../../../../tools/storage/flight/Action'
import { useDispatch, useSelector } from 'react-redux'
import TravelerForm from './Forms/travelers'
import Contact from './Forms/contact'
import Review from './Forms/review'
function FlightBooking(props) {
    const {code,fc,sid} = useParams()
    const [show, setShow] = useState(false)
    const [step, setstep] = useState(0)
    const [loading, setLoading] = useState(false)
    const history = useHistory()
    const book_info = useSelector(state => state.flight.booking)
    const [yourFlightdata, setyourFlightdata] = useState(null)
    const dispatch = useDispatch()
    useEffect(() => {
        var data = new FormData();
        data.append('fsc',code);
        data.append('src', fc);
        data.append('searchID',sid);

        setLoading(true)
            if(b2b_user_data.access_token!==null){
                if(check_token(b2b_user_data.create_time)){
                 FLIGHT_API(b2b_user_data.access_token).post('/flight-revalidate',data)
                 .then(res=>{
                     console.log(res.data)
                    setLoading(false)
                    if(res.data.Data==='Invalid'){
                     setShow(true)
                    }
                    else{
                        setyourFlightdata(res.data.Data)
                    }
                    
                    
                 })
                 .catch(err=>{
                     console.log(err)
                     setLoading(false)
                 })
                }
                else{
                    if(refresh_token(b2b_user_data.refresh_token_old)){
                 FLIGHT_API(b2b_user_data.access_token).post('/flight-revalidate',data)
                 .then(res=>{
                     console.log(res.data)
                     setLoading(false)
                     if(res.data.Data==='Invalid'){
                        setShow(true)
                       }
                       else{
                           setyourFlightdata(res.data.Data)
                            dispatch(flight_info(sid,res.data.Data.fareSourceCode,res.data.Data.fareSource,res.data.Data.pricing.totalFare,res.data.Data.pricing.currency))
                       }
                 })
                 .catch(err=>{
                     console.log(err)
                     setLoading(false)
                 })
                    }
                }
            }
     
    }, [])
    return (
        <div>
           {
               loading ? 
               <BookingLoader />:
               <div className='flight_booking_root_div'>
                   <Divider className='flight_booking_title'>FLIGHT BOOKING</Divider>
                  <Grid >
                    <Row>
                        <Col xs={24} sm={24} md={24} lg={24}>
                          <Steps  current={step}  >
                           <Steps.Item  title='Travelers' icon={<Icon icon='peoples' />} />
                           <Steps.Item  title='Contact Person' icon={<Icon icon='user' />} />
                           <Steps.Item  title='Review' icon={<Icon icon='check-circle' />} />
                          </Steps>
                        </Col>
                        <Col xs={24} sm={24} md={16} lg={16}>
                          {
                              step===0 ? <TravelerForm setstep={setstep}  />:null
                          }
                           {
                              step===1 ? <Contact setstep={setstep} userData={props.userData}  />:null
                          }
                           {
                              step===2 ? <Review setstep={setstep}   />:null
                          }
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8}>
                          {
                              yourFlightdata!==null ? 
                              <YourFlight data={yourFlightdata} />:null
                          }
                        </Col>
                    </Row>
                  </Grid>
                 
               </div>
           }
           <Modal show={show} style={{fontFamily:b2b_variables.system_font}} onHide={()=>{setShow(!show)}} >
               
               <Modal.Body>
                   <div className='flight_warning_icon'>
                       <Icon size='5x' icon='exclamation-triangle'  />
                   </div>
                   <div className='flight_warning_text'> Due to a long period of inactivity,the price of your selected flight may have changed.</div>
               </Modal.Body>
               <Modal.Footer>
                 <Button appearance='ghost' color='yellow' size='md' onClick={()=>{history.goBack()}} >Search again</Button>
               </Modal.Footer>
           </Modal>
        </div>
    )
}

export default FlightBooking
