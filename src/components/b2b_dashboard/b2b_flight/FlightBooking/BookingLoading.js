import React from 'react'
import { Icon } from 'rsuite'

function BookingLoader() {
    return (
        <div className='flight_booking_loader'>
           <div className='text'>
           <Icon spin icon='spinner' size='5x' />
           </div>
           <div className='text'>checking your flight is avaliable or not........</div>  
        </div>
    )
}

export default BookingLoader
