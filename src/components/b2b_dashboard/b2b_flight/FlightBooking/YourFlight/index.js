import React from 'react'
import { useHistory, useParams } from 'react-router-dom';
import { Button, Divider, Icon, Panel, Timeline } from 'rsuite';

function YourFlight(props) {
    const data = props.data;
    const {depfrom,goingto,adult,child,infant,depdate} = useParams()
    const history = useHistory()
    return (
        <div>
           
            <Panel className='flight_choosed_package' header={<p className='flight_choosed_packgage_heading'> {depfrom} <Icon icon='plane'  /> {goingto}  </p>}>
            <Button color='red' className='flight_booking_back_button' appearance='primary' onClick={()=>{history.goBack()}}> Back</Button>
               <p className='flight_choosed_packgage_body_text'> <Icon icon='peoples' /> {parseInt(adult)+parseInt(infant)+parseInt(child)} </p>
                 <p className='flight_choosed_packgage_body_text'> <Icon icon='calendar' /> {depdate} </p>
                
                 
                  {
                       data.itineraries ? data.itineraries.map((iti,i)=>(
                       
                        <div key={i}>
                           {
                               iti.segments.map((seg,i)=>(
                                <Timeline className='className="custom-timeline' key={i} >
                                   <Timeline.Item  dot={<Icon icon='plane' size='2x' />}  >
                                      <p className='flight_choosed_package_text_heading'> {seg.departureAirportCode} </p>
                                      <p className='flight_choosed_package_text'> {seg.departureDateTime} </p>
                                      <p className='flight_choosed_package_text'> {seg.carrierName} </p>
                                   </Timeline.Item>
                                   <Timeline.Item  dot={<Icon icon='clock-o' size='2x' />}  >
                                      <p className='flight_choosed_package_text'> {seg.journeyDuration} min </p>
                                      <p className='flight_choosed_package_text'> {seg.stopQuantity} stop </p>
                                   </Timeline.Item>
                                   <Timeline.Item  dot={<Icon icon='plane' size='2x' />}  >
                                      <p className='flight_choosed_package_text_heading'> {seg.arrivalAirportCode} </p>
                                      <p className='flight_choosed_package_text'> {seg.arrivalDateTime} </p>
                                   </Timeline.Item>
                                   </Timeline>
                               ))
                           }
                        </div>
                       )):null
                     
                   }
                
                 
            </Panel>
           
        </div>
    )
}

export default YourFlight
