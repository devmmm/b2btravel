import React from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Button, Col, ControlLabel, FormGroup, Input, Row } from 'rsuite'
import {contact_info} from '../../../../../tools/storage/flight/Action'
function Contact(props) {
    const [userId, setUserId] = useState(props.userData.ID)
    const [firstname, setFirstname] = useState(props.userData.userName)
    const [lastname, setLastname] = useState(props.userData.userName)
    const [countryCode, setCountryCode] = useState('+95')
    const [mobilenumber, setMobilenumber] = useState(props.userData.businessInfo.businessPhone)
    const [email, setEmail] = useState(props.userData.userEmail)
    const dispatch = useDispatch()
    const goReview = () =>{
      const contact =  {
        "userID": userId,
        "contactName": {
            "firstName": firstname,
            "lastName": lastname
        },
        "phone": {
            "countryCallingCode":countryCode,
            "contactNumber": mobilenumber
        },
        "emailAddress": email
    } 
    dispatch(contact_info(contact))
    props.setstep(2)
     
    }

    return (
        <div className='flight_contact_form'>
            <Row>
                <Col xs={24} sm={24} md={24} lg={24}>
                <FormGroup className='flight_contact_formgroup'>
              <ControlLabel>First Name </ControlLabel>
              <Input className='flight_contact_input' value={firstname} onChange={(v)=>{setFirstname(v)}} />
          </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col xs={24} sm={24} md={24} lg={24}>
                <FormGroup className='flight_contact_formgroup'>
              <ControlLabel>Last Name </ControlLabel>
              <Input className='flight_contact_input' value={lastname} onChange={(v)=>{setLastname(v)}} />
          </FormGroup>   
                </Col>
            </Row>
            <Row>
                <Col xs={24} sm={24} md={24} lg={24}>
                <FormGroup className='flight_contact_formgroup'>
              <ControlLabel>Email Address</ControlLabel>
              <Input className='flight_contact_input' value={email} onChange={(v)=>{setEmail(v)}} />
          </FormGroup> 
                </Col>
            </Row>
           <Row>
               <Col xs={4} sm={4} md={4} lg={4}  >
                 <FormGroup>
                    <ControlLabel> Code </ControlLabel>
                    <select className='flight_contact_select' value={countryCode} onChange={(e)=>{setCountryCode(e.target.value)}} >
                        <option className='opt_mm' value='+95'> +95 </option> 
                        <option className='opt_eng' value='+63'> +63 </option> 
                        <option className='opt_thialand' value='+98'> +98 </option> 
                    </select>
                 </FormGroup>
               </Col>
               <Col xs={20} sm={20} md={20} lg={20}  >
              <FormGroup>
                  <ControlLabel> Mobilenumber </ControlLabel>
              <Input  className='flight_contact_input' value={mobilenumber} onChange={(v)=>{setMobilenumber(v)}} />
              </FormGroup>
             
                   </Col>
                   <Col xs={24} sm={24} md={24} lg={24}>
                   <FormGroup>
               <Button onClick={()=>{props.setstep(0)}} color='orange' className='flight_previous_button' appearance='primary' > Previous </Button>
               <Button onClick={()=>{goReview()}} color='green' className='flight_next_button' appearance='primary' > Next </Button>
              </FormGroup>
                   </Col>
           </Row>
         
         
        </div>
    )
}

export default Contact
