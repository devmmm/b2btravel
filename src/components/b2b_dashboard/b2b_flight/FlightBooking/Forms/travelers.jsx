import React from 'react'
import { useParams } from 'react-router-dom'
import { Button, Col, ControlLabel, Divider, FormGroup,  Row, SelectPicker } from 'rsuite'

import { useDispatch, useSelector } from 'react-redux'
import {traveller_info_flight} from '../../../../../tools/storage/flight/Action'
import { useForm, Controller } from "react-hook-form";
function TravelerForm(props) {
    const {adult,child,infant} = useParams()
    const booking_info = useSelector(state => state.flight.booking)
    const dispatch = useDispatch()
    const { control, handleSubmit,errors } = useForm();
    const traveler = []
    const titles = [
        {
            label:'Mr',
            value:'Mr'
        },
        {
            label:'Ms',
            value:'Ms'
        },
        {
            label:'Mrs',
            value:'Mrs'
        },
    ]
    const genders = [
        {
            label:'MALE',
            value:'MALE'
        },
        {
            label:'FEMALE',
            value:'FEMALE'
        }, 
    ]
    const PassengerTypes = [
        {
            label:'Adult',
            value:'ADT'
        },
        {
            label:'Child',
            value:'CHT'
        }, 
        {
            label:'Infant',
            value:'INT'
        }, 
    ]
    const passengerNatinalitys=[
        {
            label:'Local',
            value:'MY'
        },
        {
            label:'Foreign',
            value:'FO'
        },  
    ]
    const documentTypes = [
        {
            label:'Passport',
            value:'PASSPORT'
        }, 
    ]
    const onSubmit = data => {
        let arr =  Object.keys(data).map((key) => data[key]);
         dispatch(traveller_info_flight(arr))
         props.setstep(1)

    };
    for (let index = 0; index < (parseInt(adult)+parseInt(child)+parseInt(infant)); index++) {
        var title =`[${index}][passengerName][title]`;
        var firstName=`[${index}][passengerName][firstName]`;
        var lastName=`[${index}][passengerName][lastName]`;
        var gender = `[${index}][gender]`;
        var passengerType =`[${index}][passengerType]`;
        var passengerNatinality = `[${index}][passengerNatinality]`;
        var documentType = `[${index}][passengerDocuments][documentType]`;
        var issuanceCountry = `[${index}][passengerDocuments][issuanceCountry]`;
        var documentNumber = `[${index}][passengerDocuments][documentNumber]`;
        var documentExpireDate = `[${index}][passengerDocuments][documentExpireDate]`;
        var dateOfBirth = `[${index}][dateOfBirth]`;
         traveler.push(
            
             <div className='travelers' key={index} >
                  <Row>
                  <Divider> {index+1} Traveler </Divider>
                  <Col xs={24} sm={24} md={4} lg={4}>
                      <FormGroup className='traveler_form_group'>
                          <ControlLabel>Title</ControlLabel>
                          <Controller
                            as={<SelectPicker block data={titles} searchable={false} labelKey='label' valueKey='value' />}
                            name={title}
                            control={control}
                            rules={{required:true}}
                            defaultValue='Mr'
                        />
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={10} lg={10}>
                      <FormGroup className='traveler_form_group'>
                          <ControlLabel>First Name</ControlLabel>
                          <Controller
                            as={<input required className='travelers_input' />}
                            name={firstName}
                            control={control}
                            rules={{required:true}}
                            defaultValue=''
                        />
                       
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={10} lg={10}>
                      <FormGroup className='traveler_form_group'>
                          <ControlLabel>Last Name</ControlLabel>
                          <Controller
                            as={<input required className='travelers_input' />}
                            name={lastName}
                            control={control}
                            rules={{required:true}}
                            defaultValue=''
                        />
                      </FormGroup>
                  </Col>
              </Row>
              <Row>
                  <Col xs={24} sm={24} md={6} lg={6}>
                      <FormGroup className='traveler_form_group'>
                          <ControlLabel>Gender</ControlLabel>
                          <Controller
                            as={<SelectPicker className='travelers_select' block data={genders} labelKey='label' searchable={false} valueKey='value' />}
                            name={gender}
                            control={control}
                            rules={{required:true}}
                            defaultValue='MALE'
                        />
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={6} lg={6}>
                  <FormGroup className='traveler_form_group'>
                          <ControlLabel>PassengerType</ControlLabel>
                          <Controller
                            as={<SelectPicker className='travelers_select' block data={PassengerTypes} searchable={false} labelKey='label' valueKey='value' />}
                            name={passengerType}
                            control={control}
                            rules={{required:true}}
                            defaultValue='ADT'
                        />
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={6} lg={6}>
                  <FormGroup className='traveler_form_group'>
                          <ControlLabel>passengerNatinality</ControlLabel>
                          <Controller
                            as={<SelectPicker className='travelers_select' block data={passengerNatinalitys} searchable={false} labelKey='label' valueKey='value' />}
                            name={passengerNatinality}
                            control={control}
                            rules={{required:true}}
                            defaultValue='MY'
                        />
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={6} lg={6}>
                  <FormGroup className='traveler_form_group'>
                          <ControlLabel>issuanceCountry</ControlLabel>
                          <Controller
                            as={<SelectPicker className='travelers_select' block data={passengerNatinalitys} searchable={false} labelKey='label' valueKey='value' />}
                            name={issuanceCountry}
                            control={control}
                            rules={{required:true}}
                            defaultValue='MY'
                        />
                      </FormGroup>
                  </Col>
              </Row>
              <Row>
                  <Col xs={24} sm={24} md={6} lg={6}>
                      <FormGroup className='traveler_form_group'>
                          <ControlLabel>documentType</ControlLabel>
                          <Controller
                            as={<SelectPicker className='travelers_select' block data={documentTypes} searchable={false} labelKey='label' valueKey='value' />}
                            name={documentType}
                            control={control}
                            rules={{required:true}}
                            defaultValue='PASSPORT'
                        />
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={6} lg={6}>
                  <FormGroup className='traveler_form_group'>
                          <ControlLabel>documentNumber</ControlLabel>
                            <Controller
                            as={<input required className='travelers_input' />}
                            name={documentNumber}
                            control={control}
                            rules={{required:true}}
                            defaultValue=''
                        />
                       
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={6} lg={6}>
                  <FormGroup className='traveler_form_group'>
                          <ControlLabel>documentExpireDate</ControlLabel>
                          <Controller
                            as={<input required className='travelers_input_date' type='date' />}
                            name={documentExpireDate}
                            control={control}
                            rules={{required:true}}
                            defaultValue=''
                        />
                      </FormGroup>
                  </Col>
                  <Col xs={24} sm={24} md={6} lg={6}>
                  <FormGroup className='traveler_form_group'>
                  <ControlLabel>dateOfBirth</ControlLabel>
                  <Controller
                            as={<input required className='travelers_input_date' type='date' />}
                            name={dateOfBirth}
                            control={control}
                            rules={{required:true}}
                            defaultValue=''
                        />
                      </FormGroup>
                  </Col>
              </Row>
             </div>
         )
    }
    return (
        <div>
            <form id='traveler_form' onSubmit={handleSubmit(onSubmit)}  >
              {traveler}
              <FormGroup className='traveler_form_group'>
                  <Button type='submit'   className='traveler_next_button' appearance='primary' color='green'>Next </Button>
              </FormGroup>
            </form>
        </div>
    )
}

export default TravelerForm


