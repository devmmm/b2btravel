import React from 'react'
import { useSelector } from 'react-redux'
import { Button, Col, Panel, Row,FormGroup } from 'rsuite'

function Review(props) {
    const booking_data = useSelector(state => state.flight.booking)
    const goBooking = () =>{

    }
    return (
        <div className='flight_booking_review'>
              {
                  booking_data.travelers ? booking_data.travelers.map((t,i)=>(
                    <Panel key={i} className='review_travelers' header={  'Traveler' + i+1} >
                    <div className='traveler'>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>Name </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                                {t.passengerName.title}&nbsp; {t.passengerName.firstName}&nbsp; {t.passengerName.lastName}
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>gender </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                                {t.gender}
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>passengerType </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                                {t.passengerType}   
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>passengerNatinality </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                                 {t.passengerNatinality}  
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>issuanceCountry </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                                {t.passengerDocuments.issuanceCountry}    
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>documentType </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                               {t.passengerDocuments.documentType}    
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>documentNumber </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                               {t.passengerDocuments.documentNumber}       
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>documentExpireDate </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                               {t.passengerDocuments.documentExpireDate}   
                              </p>
                           </Col>
                       </Row>
                       <Row>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>dateOfBirth </p>
                           </Col>
                           <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='flight_booking_review_flight_booking_review_label'>
                               {t.dateOfBirth}  
                              </p>
                           </Col>
                       </Row>
                    </div>
                 </Panel>
                  )):null
              }

             <Panel className='review_travelers' header='Contact Information' >
                <div className='traveler'>
                   <Row>
                       <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='flight_booking_review_flight_booking_review_label'>Name </p>
                       </Col>
                       <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='flight_booking_review_flight_booking_review_label'>
                            {booking_data.contacts.contactName.firstName} &nbsp;  {booking_data.contacts.contactName.lastName}
                          </p>
                       </Col>
                   </Row>
                   <Row>
                       <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='flight_booking_review_flight_booking_review_label'>Mobile Number </p>
                       </Col>
                       <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='flight_booking_review_flight_booking_review_label'>
                              {booking_data.contacts.phone.countryCallingCode} {booking_data.contacts.phone.countryCallingCode.contactNumber}
                          </p>
                       </Col>
                   </Row>
                   <Row>
                       <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='flight_booking_review_flight_booking_review_label'>Email Address </p>
                       </Col>
                       <Col xs={12} sm={12} md={12} lg={12}>
                           <p className='flight_booking_review_flight_booking_review_label'>
                              {booking_data.contacts.emailAddress} 
                          </p>
                       </Col>
                   </Row>
                
                </div>
             </Panel>
             <Row>
              
              <Col xs={24} sm={24} md={24} lg={24}>
              <FormGroup>
          <Button onClick={()=>{props.setstep(1)}} color='orange' className='flight_previous_button' appearance='primary' > Previous </Button>
          <Button onClick={()=>{goBooking()}} color='green' className='flight_next_button' appearance='primary' > Next </Button>
         </FormGroup>
                      </Col>
                  </Row>
        </div>
    )
}

export default Review
