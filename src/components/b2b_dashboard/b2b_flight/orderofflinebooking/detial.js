import React, { useEffect, useState } from 'react'
import { Col, ControlLabel, Divider, FormGroup, Grid, Icon, Input, Placeholder, Row } from 'rsuite'
import { FLIGHT_API } from '../../../../tools/api';
import { b2b_user_data } from '../../../../tools/constants';
import { check_token, refresh_token } from '../../../../tools/helpers';

function OfflineFlightBookingDetail(props) {
    const id = props.match.params.id;
    const [loading, setLoading] = useState(false)
    const [booking, setBooking] = useState(null)
    useEffect(() => {
        setLoading(true)
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
             FLIGHT_API(b2b_user_data.access_token).get(`/offlinebookingdetail?bookingID=${id}`)
             .then(res=>{
                 console.log(res.data)
                 setLoading(false)
                 setBooking(res.data.Data)
             })
             .catch(err=>{
                 setLoading(false)
                 console.log(err)
             })
            }
            else{
                refresh_token(b2b_user_data.refresh_token_old)
                FLIGHT_API(b2b_user_data.access_token).get(`/offlinebookingdetail?bookingID=${id}`)
             .then(res=>{
                 console.log(res.data)
                 setLoading(false)
                 setBooking(res.data.Data)
             })
             .catch(err=>{
                 setLoading(false)
                 console.log(err)
             })
            }
        }
    }, [id])
    return (
        <div>
            <Divider style={{fontWeight:'bold',fontSize:'18px',textTransform:'uppercase'}}>Offline Flight Booking Detail</Divider>
            {
                loading ? 
                <>
                   <Grid>
                      <Row>
                      <Col xs={24} sm={24} md={6} lg={6}>
                      <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                        <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                        <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                        <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                      </Row>
                      <Row>
                      <Col xs={24} sm={24} md={6} lg={6}>
                      <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                        <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                        <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                           
                            <Placeholder.Graph height={40} active style={{marginBottom:10}} />
                           
                        </Col>
                      </Row>
                      <Row>
                          <Col xs={24} sm={24} md={24} lg={24}>
                             <Placeholder.Graph height={40} active style={{marginTop:10}} />
                          </Col>
                      </Row>
                    </Grid>
                </>:
                <>
                {
                    booking!==null ?
                    <>
                    <Grid>
                      <Row>
                      <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='plane' />  AirLine : </ControlLabel>
                             <p>{booking.airline}</p>
                            </FormGroup>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='book2' />  PNR : </ControlLabel>
                            <p>{booking.pnr}</p>
                            </FormGroup>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='user' />  Pax Name : </ControlLabel>
                            <p>{booking.pax_name} </p>
                            </FormGroup>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='user' />  Pax_no : </ControlLabel>
                            <p>{booking.pax_no} </p>
                            </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                      <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='user' />  business ID : </ControlLabel>
                            <p>{booking.businessID} </p>
                            </FormGroup>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='money' />  Total Price : </ControlLabel>
                            <p> {booking.totalPrice+booking.currency} </p>
                            </FormGroup>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='history' /> Booking Status : </ControlLabel>
                            <p>{booking.bookingStatus} </p>
                            </FormGroup>
                        </Col>
                        <Col xs={24} sm={24} md={6} lg={6}>
                            <FormGroup className='offline_booking_detail'>
                            <ControlLabel> <Icon icon='history' /> Payment Status : </ControlLabel>
                             <p> {booking.paymentStatus} </p>
                            </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                          <Col xs={24} sm={24} md={24} lg={24}>
                              <p> * Remark : <small> {booking.adminRemark} </small> </p>
                          </Col>
                      </Row>
                    </Grid>
                   
                    </>:null
                }
                </>
            }
        </div>
    )
}

export default OfflineFlightBookingDetail
