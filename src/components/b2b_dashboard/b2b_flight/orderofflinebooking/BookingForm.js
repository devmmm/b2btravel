import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Col, ControlLabel, FormGroup, Grid, Icon, Input, InputNumber, Notification, Panel, Row, SelectPicker } from 'rsuite'
import OrderOfflineBooking from '.'
import { FLIGHT_API } from '../../../../tools/api'
import { b2b_user_data, b2b_variables } from '../../../../tools/constants'
import { check_token, refresh_token } from '../../../../tools/helpers'

function BookingForm() {
    const history = useHistory()
    const [pnr, setPnr] = useState('')
    const [pax_name, setPax_name] = useState('')
    const [pax_no, setPax_no] = useState(1)
    const [remark, setRemark] = useState('')
    const [loading, setLoading] = useState(false)
    const [selectedAirline, setSelectedAirline] = useState('')
    const Airlines = [
        {
            name:'MNA-USD',
            label:'MNA-USD'
        },
        {
            name:'KBZ',
            label:'KBZ'
        },
        {
            name:'GMA-USD',
            label:'GMA-USD'
        },
        {
            name:'GMA-MMK',
            label:'GMA-MMK' 
        }

    ]
    const OrderOfflineBooking = () =>{
        setLoading(true)
        var data = new FormData();
       
        data.append('airline', selectedAirline);
        data.append('pnr', pnr);
        data.append('pax_name', pax_name);
        data.append('pax_no', pax_no);
        data.append('remark', remark);
        if(b2b_user_data.access_token!==null){
            if(check_token(b2b_user_data.create_time)){
             FLIGHT_API(b2b_user_data.access_token).post('/orderofflinebooking',data)
             .then(res=>{
                 console.log(res.data)
                 setLoading(false)
                 if(res.data.Status.Code==='01'){
                    Notification.success({
                        title:'b2b.travels.com.mm',
                        description:(
                            <div className='flight_booking_offline'>
                                <p>Booking Success!</p>
                                <Button size='sm' onClick={()=>{history.push(`/dashboard/flight/booking/flight_booking_detial/${res.data.Data}`)}} appearance='ghost' color='orange' className='view_booking' >View your Booking</Button>
                            </div>
                        ),
                        style:{
                            fontFamily:b2b_variables.system_font
                        },
                        duration:10000
                    })
                    setSelectedAirline('')
                    setPnr('')
                    setPax_name('')
                    setRemark('')
                }
                else{
                    Notification.error({
                        title:'b2b.travels.com.mm',
                        description:
                           (
                               <div>
                                   <p>Booking Error!</p>
                               </div>
                           )
                        ,
                        style:{
                            fontFamily:b2b_variables.system_font
                        },
                        duration:10000
                    }) 
                }
             })
             .catch(err=>{
                 console.log(err)
                 setLoading(false)
             })
            }
            else{
                refresh_token(b2b_user_data.refresh_token_old)
                FLIGHT_API(b2b_user_data.access_token).post('/orderofflinebooking',data)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    if(res.data.Status.Code==='01'){
                        Notification.success({
                            title:'b2b.travels.com.mm',
                            description:(
                                <div className='flight_booking_offline'>
                                    <p>Booking Success!</p>
                                    <Button size='sm' onClick={()=>{history.push(`/dashboard/flight/booking/flight_booking_detial/${res.data.Data}`)}} appearance='ghost' color='orange' className='view_booking' >View your Booking</Button>
                                </div>
                            ),
                            style:{
                                fontFamily:b2b_variables.system_font
                            },
                            duration:10000
                        })
                        setSelectedAirline('')
                        setPnr('')
                        setPax_name('')
                        setRemark('')
                    }
                    else{
                        Notification.error({
                            title:'b2b.travels.com.mm',
                            description:'Book error!',
                            style:{
                                fontFamily:b2b_variables.system_font
                            },
                            duration:10000
                        }) 
                    }
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })
            }
        }
    }
    return (
        <div>
           <Grid>
               <Row>
                   <Col xs={24} sm={24} md={24} lg={24}>
                       <Panel style={{minHeight:100,background:'#f2f2f2'}}>
                          <Row>
                          <Col xs={24} sm={24} md={6} lg={6}>
                             <FormGroup>
                                    <ControlLabel> <Icon icon='plane' /> Airline </ControlLabel>
                                     <SelectPicker data={Airlines} labelKey='label' valueKey='name' searchable={false} block value={selectedAirline} onChange={(v)=>{setSelectedAirline(v)}} appearance='default' />
                                </FormGroup>
                              </Col>
                              <Col xs={24} sm={24} md={6} lg={6}>
                                <FormGroup>
                                    <ControlLabel> <Icon icon='book2' /> PNR No. </ControlLabel>
                                    <Input value={pnr} onChange={(v)=>{setPnr(v)}} />
                                </FormGroup>
                             </Col>
                              <Col xs={24} sm={24} md={6} lg={6}>
                                <FormGroup>
                                    <ControlLabel><Icon icon='user' /> Pressenger Name :</ControlLabel>
                                    <Input value={pax_name} onChange={(v)=>{setPax_name(v)}} />
                                </FormGroup>
                              </Col>
                              
                             <Col xs={24} sm={24} md={6} lg={6}>
                             <FormGroup>
                                    <ControlLabel> <Icon icon='user' />  No. of Persons </ControlLabel>
                                    <InputNumber min={1} value={pax_no} onChange={(v)=>{setPax_no(v)}} />
                                </FormGroup>
                              </Col>

                              <Col xs={24} sm={24} md={24} lg={24}>
                                 <FormGroup>
                                     <ControlLabel>Remark :</ControlLabel>
                                     <Input className='remark_text_area' value={remark} onChange={(v)=>setRemark(v)} />
                                 </FormGroup>
                              </Col>
                              <Col xs={24} sm={24} md={24} lg={24}>
                                 <FormGroup style={{marginTop:10}}>
                                   <Button disabled={selectedAirline==='' || pnr ===''|| pax_name ===''|| pax_no==='' || remark==='' ? true:false} loading={loading} onClick={()=>{OrderOfflineBooking()}} appearance='primary' size='lg' color='green'> Book </Button>
                                 </FormGroup>
                              </Col>
                          </Row>
                       </Panel>
                   </Col>
               </Row>
           </Grid>
        </div>
    )
}

export default BookingForm
