import React from 'react'
import { Divider } from 'rsuite'
import BookingForm from './BookingForm'

function OrderOfflineBooking() {
    return (
        <div>
            <Divider>Order Offline Booking</Divider>
            <BookingForm />
        </div>
    )
}

export default OrderOfflineBooking
