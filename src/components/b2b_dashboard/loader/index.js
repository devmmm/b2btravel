import React from 'react'
import { Icon } from 'rsuite'

function LoadPage() {
    return (
        <div style={{minHeight:'100vh',justifyItems:'center',justifyContent:'center',alignItems:'center',alignContent:'center',display:'flex'}}>
            <Icon size='5x' icon='refresh' spin />
        </div>
    )
}

export default LoadPage
