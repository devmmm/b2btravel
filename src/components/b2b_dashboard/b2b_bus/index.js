import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import BusHome from './home'
import SearchHome from './search_home'
import BusList from './bus_lists'
import Booking from './booking'
import BusBookingList from './bus_booking_list'
import BookingDetial from './booking/detial'

function B2BBus(props) {
   
    return (
       <Switch>
           <Route exact path={`${props.route}`} component={BusHome} />
           <Route exact path={`${props.route}/bus_search`} component={SearchHome} />
           <Route exact path={`${props.route}/bus_search/:leavingfrom/:goingto/:depatruedate/:noofseat/:nationality`} component={BusList} />
           <Route exact path ={`${props.route}/booking/:leavingfrom/:goingto/:departure_date/:no_of_seats/:nationality`} component={Booking}/>
           <Route exact path={`${props.route}/booking/booking_list`} component={BusBookingList} />
           <Route exact path={`${props.route}/booking/bus_booking_detial/:id`} component={BookingDetial} />
           <Redirect to='/404' />
       </Switch>
    )
}

export default B2BBus
