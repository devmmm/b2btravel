import React, { useState, useEffect } from 'react'
import BusSearchBox from './BusSearchBox'
import { Divider, Icon, Grid, Row, Panel, Col,Button, Placeholder, Modal, Carousel, Notification,ButtonToolbar } from 'rsuite'
import { check_token, refresh_token } from '../../../../tools/helpers';
import { BUS_API } from '../../../../tools/api';
import {timeConvert} from '../../../../tools/helpers/'
import { useHistory } from 'react-router-dom';
import { b2b_variables } from '../../../../tools/constants';
function BusList(props) {
  let leavingfrom = props.match.params.leavingfrom;
   let goingto = props.match.params.goingto;
   let depatruedate = props.match.params.depatruedate;
   let noofseat = props.match.params.noofseat;
  let  nationality = props.match.params.nationality;
  const [lists, setLists] = useState([])
  const [loading, setLoading] = useState(false)
  const access_token = localStorage.getItem('access_token');
  const create_time = localStorage.getItem('create_time');
  const refresh_token_old = localStorage.getItem('refresh_token')
  const [operatorPhotos, setoperatorPhotos] = useState([])
  const [show, setShow] = useState(false)
  const history = useHistory()
  useEffect(() => {
    var formdata = new FormData();
    formdata.append("depfrom",leavingfrom);
    formdata.append("arrvto", goingto);
    formdata.append("depdate", depatruedate);
    formdata.append("pax", noofseat);
    formdata.append("nationality", nationality);

    if(access_token!==null){
        setLoading(true)
        if(check_token(create_time)){
          BUS_API(access_token).post('/searchbus',formdata)
          .then(res=>{
              console.log(res.data)
              setLoading(false)
              setLists(res.data.Data.results)
              if(res.data.Data.results.length===0){
               Notification.warning({
                 title:'b2b.travels.com.mm',
                 duration:10000,
                 description:(
                   <div style={{padding:10,lineHeight:2}}>
                     <p>No Data Found!</p>
                     <ButtonToolbar>
                      <Button
                        onClick={() => {
                          Notification.close();
                        }}
                      >
                        OK
                      </Button>
                      <Button
                        onClick={() => {
                          Notification.close();
                        }}
                      >
                        Cancel
                      </Button>
                    </ButtonToolbar>
                   </div>
                 ),
                 style:{
                   fontFamily:b2b_variables.system_font
                 }
                 
               })
              }
          })
          .catch(err=>{
              setLoading(false)
              console.log(err)
          })
        }
        else{
            if(refresh_token(refresh_token_old)){
                BUS_API(access_token).post('/searchbus',formdata)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    setLists(res.data.Data.results)
                })
                .catch(err=>{
                    setLoading(false)
                    console.log(err)
                })
            }
        }
    }
  }, [leavingfrom,goingto,depatruedate,noofseat,nationality])
  const busBooking = (selected_bus,seatPlanUrl) =>{
    localStorage.setItem('selectedbus',JSON.stringify(selected_bus))
    localStorage.setItem('seatPlanUrl',seatPlanUrl)
    history.push(`/dashboard/bus/booking/${leavingfrom}/${goingto}/${depatruedate}/${noofseat}/${nationality}`)
  }

  const bus_Operator_Photos = (photos) =>{
    setoperatorPhotos(photos)
    setShow(true)
  }
    return (
        <div>
           <BusSearchBox leavingfrom={leavingfrom} goingto={goingto} depatruedate={depatruedate} noofseat={noofseat} nationality={nationality}   />
           <Divider>  
           <p> <small> <Icon icon='calendar' /> {depatruedate} | <Icon icon='user' />{noofseat} | <Icon icon='user-analysis' />{nationality} </small></p>
           </Divider>
           {
               loading ? 
               <Grid>
               <Row>
               <Col>
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
                <Placeholder.Graph height={120} active  style={{marginBottom:10}} />
               </Col>
               </Row>
           </Grid>:
           <Grid>
           <Row>
           <Col>
           {
            lists.map((b,i)=>(
         <Panel style={{background:'#f2f2f2',marginBottom:10}}  header={b.trip.operatorNameEN} key={i} >
          <Row>
          <Col xs={24} sm={24} md={8} lg={8}>
        <div>
          <p style={{fontWeight:'bold'}}> {new Date(b.trip.departureDateTime).toLocaleString()} - {b.trip.busTypeNameEN}</p>
           <small> {b.trip.sourceNameEN}-{b.trip.destinationNameEN}</small>
           <small> Departs:{new Date(b.trip.departureDateTime).toLocaleString()} </small>
          <small> Arrives: {new Date(b.trip.estArrivalDateTime).toLocaleString()}  Duration: { timeConvert((new Date(b.trip.estArrivalDateTime).getTime())-(new Date(b.trip.departureDateTime).getTime())) } </small>

       </div>  
    </Col>
    <Col xs={24} sm={24} md={8} lg={8}>
        <div style={{textAlign:'center'}}>
          <img src={process.env.PUBLIC_URL+"/"+b.operatorLogoUrl} style={{width:100,height:50}} alt='buslogo' />
          <div style={{marginTop:10}}>
          <Button appearance='ghost'  size='sm' color='orange' onClick={()=>{bus_Operator_Photos(b.photoSources)}} > <Icon icon='camera' /> Operator Photos</Button>
          </div>
        </div>
  </Col>
  <Col xs={24} sm={24} md={8} lg={8}>
  <div style={{textAlign:'center'}}>
      <p style={{fontWeight:'bold',color:'tomato'}}> {parseInt(b.trip.localTicketPrice.amount)*noofseat} {b.trip.localTicketPrice.currency} </p>
      <p> <small>{noofseat} Seats x {b.trip.localTicketPrice.amount} </small></p>
      <Button  color='orange' size='sm' appearance='default' onClick={()=>{busBooking(b.trip,b.seatPlanUrl)}}>Select Seats</Button>
   </div>
  </Col>
          </Row>
        </Panel>
            ))
          }
           </Col>
           </Row>
           <Modal show={show} onHide={()=>{setShow(!show)}} backdrop   >
             <Modal.Title>Photos</Modal.Title>
             <Modal.Body>
              <Carousel className="custom-slider" >
               {
                 operatorPhotos.map((photo,i)=>(
                  <img key={i} src={photo} alt='operator_photo' height='300' />
                 ))
               }
              </Carousel>
             </Modal.Body>
           </Modal>
       </Grid>
           }
          
        </div>
    )
}

export default BusList
