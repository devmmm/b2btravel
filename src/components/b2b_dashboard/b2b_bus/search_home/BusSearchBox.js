import React,{ useState, useEffect } from 'react'
import { Grid, Row, Panel, Divider, Col, FormGroup, ControlLabel, Icon, InputPicker, DatePicker, Button, Placeholder } from 'rsuite';
import { useHistory } from 'react-router-dom';
import { formatDate, check_token, refresh_token } from '../../../../tools/helpers';
import { BUS_API } from '../../../../tools/api';

function BusSearchBox(props) {
    const [leavingfrom, setLeavingfrom] = useState()
    const [goingto, setGoingto] = useState('')
    const [departure_date, setDeparture_date] = useState('')
    const [noseats, setNoseats] = useState('')
    const [nationality, setNationality] = useState('LOCAL')
    const [leavingDest, setLeavingDest] = useState([])
    const [goingDest, setGoingDest] = useState([])
    const [loading, setLoading] = useState(false)
    const access_token = localStorage.getItem('access_token');
    const create_time = localStorage.getItem('create_time');
    const refresh_token_old = localStorage.getItem('refresh_token')
    useEffect(() => {
       if(access_token!==null){
           setLoading(true)
           if(check_token(create_time)){
               BUS_API(access_token).get('/destinations')
               .then(res=>{
                   console.log(res.data)
                   setLeavingDest(res.data.Data)
                   setGoingDest(res.data.Data)
                   setLoading(false)
               })
               .catch(err=>{
                   console.log(err)
                   setLoading(false)
               })
               
           }
           else{
               if(refresh_token(refresh_token_old)){
                BUS_API(access_token).get('/destinations')
                .then(res=>{
                    console.log(res.data)
                    setLeavingDest(res.data.Data)
                    setGoingDest(res.data.Data)
                    setLoading(false)
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })  
               }
           }
       }
    }, [])
    const seats = [
        {
            label:'1',
            value:'1'
        },
        {
            label:'2',
            value:'2'
        },
        {
            label:'3',
            value:'3'
        },
        {
            label:'4',
            value:'4'
        },
        {
            label:'5',
            value:'5'
        },
        {
            label:'6',
            value:'6'
        },
        
    ]
    const nations = [
        {
            label:'LOCAL',
            value:'LOCAL'
        },
        {
            label:'FOREIGN',
            value:'FOREIGN'
        },
    ]
   let history = useHistory()
   const searchBus = () =>{
    history.push(`/dashboard/BUS/bus_search/${leavingfrom}/${goingto}/${formatDate(departure_date)}/${noseats}/${nationality}`)
   }
    return (
        <div style={{marginTop:10,padding:10}}>
            <Divider>BUS SEARCH</Divider>
            <Panel style={{background:'#f2f2f2'}}>
            <Grid>
                <Row>
                    <Col xs={24} sm={24} md={4} lg={4}>
                      <FormGroup style={{marginBottom:10}}>
                          <ControlLabel><Icon icon='map-marker' />Leaving From </ControlLabel>
                          {
                              loading ? <Placeholder.Graph height={30} active />:<InputPicker appearance='subtle' valueKey='pk' labelKey='destinationNameEN' value={leavingfrom} onChange={(v)=>{setLeavingfrom(v)}} data={leavingDest} block  />
                          }
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={24} md={4} lg={4}>
                      <FormGroup style={{marginBottom:10}}>
                          <ControlLabel><Icon icon='map-marker' />Going To </ControlLabel>
                         {
                             loading ? <Placeholder.Graph  active height={30} />:
                             <InputPicker  appearance='subtle'  value={goingto} valueKey='pk' labelKey='destinationNameEN'  onChange={(v)=>{setGoingto(v)}}  data={goingDest} block  />
                         }
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={24} md={4} lg={4}>
                      <FormGroup style={{marginBottom:10}}>
                          <ControlLabel><Icon icon='calendar' />Departure Date </ControlLabel>
                          <DatePicker appearance='subtle' block value={departure_date} onChange={(v)=>{setDeparture_date(v)}} />
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={24} md={4} lg={4}>
                    <FormGroup style={{marginBottom:10}}>
                          <ControlLabel><Icon icon='user' />No. of Seats </ControlLabel>
                          <InputPicker appearance='subtle'  value={noseats} onChange={(v)=>{setNoseats(v)}} data={seats} block  />
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={24} md={4} lg={4}>
                    <FormGroup style={{marginBottom:10}}>
                          <ControlLabel><Icon icon='user-analysis' />Nationality </ControlLabel>
                          <InputPicker appearance='subtle'  value={nationality} onChange={(v)=>{setNationality(v)}} data={nations}  block  />
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={24} md={4} lg={4}>
                    <FormGroup style={{marginBottom:10,marginTop:20}}>
                         <Button disabled={leavingfrom===''|| goingto===''||departure_date===''||noseats===''||nationality===''?true:false} onClick={()=>{searchBus()}} appearance='primary' size='lg' block color='green'>Search</Button>
                      </FormGroup>
                    </Col>
                </Row>
            </Grid>
            </Panel>
        </div>
    )
}

export default BusSearchBox
