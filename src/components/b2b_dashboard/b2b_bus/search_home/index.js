import React from 'react'
import { Switch, Route } from 'react-router-dom'
import BusSearchBox from './BusSearchBox'


function SearchHome({match}) {
    
    return (
       <Switch>
           <Route exact path={`${match.path}`} component={BusSearchBox} />
       </Switch>
    )
}

export default SearchHome
