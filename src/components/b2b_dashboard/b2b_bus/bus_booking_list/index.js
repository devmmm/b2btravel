import React, { useState, useEffect } from 'react'
import { b2b_user_data } from '../../../../tools/constants'
import { check_token, refresh_token } from '../../../../tools/helpers'
import { BUS_API } from '../../../../tools/api'
import { Divider, Grid, Col, Button, Panel, Icon, Placeholder } from 'rsuite'
import { useHistory } from 'react-router-dom'

function BusBookingList() {
    const [bookings, setBookings] = useState([])
    const [loading, setLoading] = useState(false)
   
    const history = useHistory()
    useEffect(() => {
        setLoading(true)
       if(b2b_user_data.access_token!==null){
        if(check_token(b2b_user_data.create_time)){
          BUS_API(b2b_user_data.access_token).get('/busbookings')
          .then(res=>{
              console.log(res.data)
              setBookings(res.data.Data)
              setLoading(false)
          })
          .catch(err=>{
              console.log(err)
              setLoading(false)
          })
        }
       }
       else{
           if(refresh_token(b2b_user_data.refresh_token_old)){
            BUS_API(b2b_user_data.access_token).get('/busbookings')
            .then(res=>{
                console.log(res.data)
                setBookings(res.data.Data)
                setLoading(false)
            })
            .catch(err=>{
                console.log(err)
                setLoading(false)
            })
           }
       }
    }, [b2b_user_data.create_time])
    return (
        <div>
            <Divider>MY BUS BOOKINGS</Divider>
            {
              loading ?
              <Grid >
                  <Col sm={24} xs={24} md={24} lg={24} >
                  <div className='bus_booking_detial_loader'>
                     <Icon spin icon='spinner' size='2x' />
                  </div>
                  </Col>
             
          </Grid>:
           <Grid >
          
           {
             bookings.map((b,i)=>(
               <Col key={i} sm={24} xs={24} md={24} lg={24} >
               <Panel  className='my_bus_booking' header={<p className='bus_booking_heading'> {b.depdate + "," + b.deptime + "|" + b.depfrom + '-' + b.arrvto }</p>} >
                 <p className='bus_traveller_name'><Icon icon='user' /> Traveller Name : {b.pax_name} </p>
                 <p className='bus_ref'> <Icon icon='car' /> Ref# {b.fareSource} | {b.operatorcode}  </p>
                 <p className='bus_seats'> <Icon icon='wheelchair' /> Seat Numbers : {b.seats} | Amount : {b.perpaxprice}MMK </p>
                 <p className='book_date'> <Icon icon='calendar' /> Booked -  {new Date(b.bookingDate).toLocaleString()} </p>
                 <Button onClick={()=>{history.push(`/dashboard/bus/booking/bus_booking_detial/${b.ID}`)}} appearance='primary' size='md' color='green' className='bus_booking_view_detial'>Detial</Button>
               </Panel>
               </Col>
             ))
           }
         
       </Grid>
            }
            <pre>{JSON.stringify(bookings,null,2)}</pre>
           
        </div>
    )
}

export default BusBookingList
