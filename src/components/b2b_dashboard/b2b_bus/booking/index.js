import React, { useState } from 'react'
import { Divider, Steps, Icon, Grid, Row, Col } from 'rsuite'
import BookingFrom from './forms'
import TripInfo from './tripinfo'

function Booking(props) {
    const tripInfo = JSON.parse(localStorage.getItem('selectedbus'))
    const [step, setStep] = useState(0)
    let not = props.match.params.no_of_seats
    let nationality = props.match.params.nationality
    let leavingfrom = props.match.params.leavingfrom;
    let goingto = props.match.params.goingto;
    return (
        <div>
            <Divider> BUS BOOKING</Divider>
             <Steps  current={step} >
              <Steps.Item title='Seat Selection' icon={<Icon icon='crosshairs' />} />
              <Steps.Item title='Traveler Information' icon={<Icon icon='user' />} />
              <Steps.Item title='Confrimation' icon={<Icon icon='check' />} />
             </Steps>
           <Grid >
            <Row>
                <Col xs={24} sm={24} md={16} lg={16}>
                <BookingFrom tripInfo={tripInfo} not={not} step={step} setStep={setStep} nationality={nationality} leavingfrom={leavingfrom} goingto={goingto} />
                </Col>
                <Col xs={24} sm={24} md={8} lg={8}>
                 <TripInfo tripInfo={tripInfo} not={not} />
                 
                </Col>
            </Row>
           </Grid>
        </div>
    )
}

export default Booking
