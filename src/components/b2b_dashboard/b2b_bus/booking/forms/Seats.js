import React, { useState, useEffect } from 'react'
import { b2b_user_data } from '../../../../../tools/constants'
import { check_token, refresh_token, formatDate } from '../../../../../tools/helpers'
import { BUS_API } from '../../../../../tools/api'
import {Row,Col,Placeholder,Panel,CheckboxGroup,Checkbox,Icon,FormGroup,Button} from 'rsuite'
import {trip_info, seat_selection} from '../../../../../tools/storage/bus/booking/Action'
import { useDispatch } from 'react-redux'
function Seats(props) {
    let tripInfo=props.tripInfo 
    let not=props.not 
    let nationality=props.nationality
    let setStep=props.setStep
    const [seatplan, setSeatplan] = useState([])
    const [loading, setLoading] = useState(false)
    const [selectedSeat, setSelectedSeat] = useState([])
    const dispatch = useDispatch()
    useEffect(() => {
        let data = new FormData();
    data.append('depfrom',props.leavingfrom);
    data.append('arrvto', props.goingto);
    data.append('depdate', formatDate(new Date(tripInfo.departureDateTime)));
    data.append('pax', not);
    data.append('nationality', nationality);
    data.append('operatorcode', tripInfo.operatorcode);
    data.append('seatPlanUrl', localStorage.getItem('seatPlanUrl'));
    data.append('tripID', tripInfo.correlationId);
      if(b2b_user_data.access_token!==null){
          setLoading(true)
          if(check_token(b2b_user_data.create_time)){
              BUS_API(b2b_user_data.access_token).post('/getseatplan',data)
              .then(res=>{
                  console.log(res.data)
                  setSeatplan(res.data.Data)
                  setLoading(false)
              })
              .catch(err=>{
                  console.log(err)
                  setLoading(false)
              })
          }

          else{
              if(refresh_token(b2b_user_data.refresh_token_old)){
                BUS_API(b2b_user_data.access_token).post('/getseatplan',data)
              .then(res=>{
                  console.log(res.data)
                  setSeatplan(res.data.Data)
                  setLoading(false)
              })
              .catch(err=>{
                  console.log(err)
                  setLoading(false)
              })
              }
              else{
                BUS_API(b2b_user_data.access_token).post('/getseatplan',data)
                .then(res=>{
                    console.log(res.data)
                    setSeatplan(res.data.Data)
                    setLoading(false)
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })
              }
          }
      } 
    }, [b2b_user_data.create_time])
    const ContinuetoTraveller = () =>{
        var trip_data = {
            operatorcode:tripInfo.operatorcode,
            seatPlanUrl:localStorage.getItem('seatPlanUrl'),
            tripID:tripInfo.correlationId,
            depfrom:props.leavingfrom,
            arrvto:props.goingto,
            depdate:formatDate(tripInfo.departureDateTime),
            deptime:new Date(tripInfo.departureDateTime).toLocaleTimeString(),
            nationality:nationality,
            perpaxprice:{
              amount:tripInfo.localTicketPrice.amount,
              currency:tripInfo.localTicketPrice.currency
            }
        }
        dispatch(trip_info(trip_data))
        dispatch(seat_selection(selectedSeat))
        setStep(1)
    }
    return (
        <div style={{marginTop:10,marginBottom:10}}>
            <Row>
             <Col sm={24} xs={24} md={24} lg={24}>
                {
                  loading ? 
                  <>
                  <Placeholder.Graph active height={400} />
                  </>:
                  <>
                   <Panel style={{background:'f2f2f2',minHeight:40}}>
                    <CheckboxGroup  value={selectedSeat} onChange={(v)=>{setSelectedSeat(v)}} >
                       
                         {
                             seatplan.map((s,i)=>(
                                <Row key={i}>
                                  {
                                      s.map((d,i)=>(
                                        <Col key={i}  sm={4} xs={4} md={4} lg={4}> {d.isSeat ? <Checkbox   disabled={!d.isAvailable||selectedSeat.length===parseInt(not)} value={d.number}  style={d.isAvailable ? {background:'#66bb6a',textAlign:'center',marginTop:10,color:'#fff'}:{background:'#ef9a9a',textAlign:'center',marginTop:10,color:'#fff'}} >{!d.isAvailable ? <Icon icon='lock' />:null} {d.number} </Checkbox>:null}
                                         </Col>
                                        // <Col key={i}  sm={4} xs={4} md={4} lg={4}> {d.isSeat  ? <Button disabled={!d.isAvailable} onClick={()=>{chooseSeat(d.number)}} appearance={d.isAvailable  ? "ghost":"primary"} style={{marginTop:10}} block>   {d.number} </Button>:null }
                                        //  </Col>
                                      ))
                                  }
                                </Row>
                             ))
                         }
                       
                      
                       
                    </CheckboxGroup>
                    <FormGroup style={{marginTop:10}}>
                     <Button color='yellow' style={{marginRight:10}} onClick={()=>{setSelectedSeat([])}} >Reset</Button>
                    <Button disabled={selectedSeat.length!==parseInt(not)} onClick={()=>{ContinuetoTraveller()}} appearance='primary'>Continue</Button>
                    </FormGroup>
                   
                   
                 </Panel>
                  </>
                }
             </Col>
         </Row>
        </div>
    )
}

export default Seats
