import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { FormGroup, Button,  Row, Col, Divider, Icon, Message } from 'rsuite'
import { useHistory } from 'react-router-dom'
import { b2b_user_data } from '../../../../../tools/constants'
import { refresh_token, check_token } from '../../../../../tools/helpers'
import { BUS_API } from '../../../../../tools/api'

function Review(props) {
    const booking_data = useSelector(state => state.bus_booking.booking_data)
   
    const [loading, setLoading] = useState(false)
    const [success_book, setSuccess_book] = useState(false)
    const [fail_book, setFail_book] = useState(false)
    const [show_review, setShow_review] = useState(true)
    const [bookingId, setbookingId] = useState('')
    const history = useHistory()
const goBooking = () =>{
    if(b2b_user_data.access_token!==null){
        if(check_token(b2b_user_data.create_time)){
         setLoading(true)
        
         BUS_API(b2b_user_data.access_token).post('/bookbusticket',booking_data)
         .then(res=>{
             console.log(res.data)
             setLoading(false)
             if(res.data.Status.Code==='200'){
                
                setSuccess_book(true)
                setShow_review(false)
                setbookingId(res.data.Data.bookingID)
             }
             
         })
         .catch(error=>{
             console.log(error)
             setLoading(false)
             setFail_book(true)
             setShow_review(false)
         })
        }
        else{
            if(refresh_token(b2b_user_data.refresh_token_old)){
             BUS_API(b2b_user_data.access_token).post('/bookbusticket',booking_data)
             .then(res=>{
                 console.log(res.data)
                 setLoading(false)
                 setSuccess_book(true)
                 setShow_review(false)
                 setbookingId(res.data.Data.bookingID)
                
             })
             .catch(error=>{
                 console.log(error)
                 setLoading(false)
                 setFail_book(true)
                 setShow_review(false)
             })
            }
            else{
                BUS_API(b2b_user_data.access_token).post('/bookbusticket',booking_data)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    setSuccess_book(true)
                    setShow_review(false)
                    setbookingId(res.data.Data.bookingID)
                   
                })
                .catch(error=>{
                    console.log(error)
                    setLoading(false)
                    setFail_book(true)
                    setShow_review(false)
                })
            }
        }
    }
}
const ViewBooking = (bookingId) =>{
  history.push(`/dashboard/bus/booking/bus_booking_detial/${bookingId}`)
}
const FailBooking = () =>{
     props.setStep(0)
}
    return (
        <div style={{marginBottom:10}}>
          
          {
              show_review ?
              <>

              <Row>
                       <Col sm={24} xs={24} md={24} lg={24} >
                           
                        <Divider style={{textTransform:'uppercase'}}> Booker Info </Divider>
                           <p>  Pax Name   &nbsp; &nbsp;  {booking_data.pax_name}  </p>
                           <p>  Pax ContactNo.   &nbsp; &nbsp;  {booking_data.pax_contactno} </p>
                           <p>  Pax EmailAddress   &nbsp; &nbsp;  {booking_data.pax_email}  </p>
                           <p>  Gender   &nbsp; &nbsp;  {booking_data.pax_gender}  </p>
                           <p>  ID   &nbsp; &nbsp;   {booking_data.pax_ID}  </p>
                           <p>  Pax Remark   &nbsp; &nbsp;   {booking_data.pax_remark}  </p>
                           <p>  Pax No.   &nbsp; &nbsp;   {booking_data.pax_no} </p>
                       </Col>
                       <Col sm={24} xs={24} md={24} lg={24} >
                       <Divider style={{textTransform:'uppercase'}}> Trip Info </Divider>
                       <p>  Operator Code  &nbsp; &nbsp;   {booking_data.operatorcode} </p>
                       <p>  Trip ID  &nbsp; &nbsp;  {booking_data.tripID} </p>
                       <p>  Depart From  &nbsp; &nbsp;   {booking_data.depfrom} </p>
                       <p>  Arrive To   &nbsp; &nbsp;  {booking_data.arrvto} </p>
                       <p>  Departure Date  &nbsp; &nbsp;   {booking_data.depdate}</p>
                       <p>  Departure Time  &nbsp; &nbsp;   {booking_data.deptime} </p>
                       <p>   Seats &nbsp; &nbsp;   {booking_data.seats.map((s,i)=>(
                           <span key={i}>{s} ,</span>
                       ))} </p>
                       </Col>
                       <Col sm={24} xs={24} md={24} lg={24} >
                       <Divider style={{textTransform:'uppercase'}}> Price  </Divider>
                       <p>  perpaxprice  &nbsp; &nbsp;   {booking_data.perpaxprice.amount} {booking_data.perpaxprice.currency}  </p>
                       <p>  No. of Pax  &nbsp; &nbsp;  {localStorage.getItem('no_of_persons')} </p>
                       <p>  Total  &nbsp; &nbsp;  {parseInt(localStorage.getItem('no_of_persons')*booking_data.perpaxprice.amount)} {booking_data.perpaxprice.currency}  </p>
                       </Col>
                     
   
                   </Row>
               
             
              <FormGroup style={{marginTop:10}} >
                   <Button  onClick={()=>{props.setStep(1)}} style={{marginRight:10}}>Back</Button> <Button loading={loading} onClick={()=>{goBooking()}} appearance='primary' >Confrim</Button>
              </FormGroup>
              </>:null
          }
          {
              success_book ? 
              <>
              <Row style={{marginTop:20}}>
                  <Col  sm={24} xs={24} md={24} lg={24}  >
                    <Message  type='success'
                     showIcon title='success' description={
                         <>
                         <p>Thank you for your booking. your booking is successful.</p>
                         <div style={{marginTop:10}}>
                            <Button onClick={()=>{ViewBooking(bookingId)}}  appearance='subtle' >view your booking</Button>
                         </div>
                         </>
                     }
                    />
                  </Col>
              </Row>
              </>:null
          }
            {
              fail_book ? 
              <>
              <Row>
                  <Col  sm={24} xs={24} md={24} lg={24}  >
                    <Message  type='error'
                     showIcon title='fail' description={
                         <>
                         <p>Thank you for your booking. your booking is unsuccessful.</p>
                         <div style={{marginTop:10}}>
                            <Button onClick={()=>{FailBooking()}}  appearance='subtle' >Back</Button>
                         </div>
                         </>
                     }
                    />
                  </Col>
              </Row>
              </>:null
          }
        </div>
    )
}

export default Review
