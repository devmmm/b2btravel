import React, { useState } from 'react'
import { Row, Col, FormGroup, ControlLabel, Input,InputPicker, Button, Alert } from 'rsuite'
import { useDispatch } from 'react-redux'
import { ValidateEmail, Validatephonenumber, } from '../../../../../tools/helpers'
import { traveller_info } from '../../../../../tools/storage/bus/booking/Action'

function Traveler(props) {
    const dispatch = useDispatch()
    const [tname, setTname] = useState('')
    const [gender, setGender] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [note, setNote] = useState('')
    const [id, setId] = useState('')

    const gender_data = [
        {
            label:'Male',
            value:'male'
        },
        {
            label:'Female',
            value:'female'
        },
        {
            label:'Other',
            value:'other'
        },
    ]
    const backStep = () =>{
     props.setStep(0)
    }
    const ContinueReview = () =>{
     if(!ValidateEmail(email)){
         Alert.error('Invalid Email Address!')
     }
    else if(!Validatephonenumber(phone)){
        Alert.error('Invalid Mobile Number!')
     }
     else{
         var t_info = {
            pax_name:tname,
            pax_contactno:phone,
            pax_email:email,
            pax_gender:gender,
            pax_ID: id,
            pax_remark: note,
         }
         dispatch(traveller_info(t_info))
         props.setStep(2)
     }
   
    }
    return (
        <div style={{marginTop:10,marginBottom:10}}>
            <Row>
                <Col sm={24} md={24} lg={24} xs={24}>
                    <FormGroup style={{marginTop:10}}>
                        <ControlLabel>Traveler Name: </ControlLabel>
                        <Input value={tname} onChange={(v)=>{setTname(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginTop:10}}>
                        <ControlLabel>Traveler ID: </ControlLabel>
                        <Input value={id} onChange={(v)=>{setId(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginTop:10}}>
                        <ControlLabel>Gender: </ControlLabel>
                        <InputPicker value={gender} onChange={(v)=>{setGender(v)}} block data={gender_data} labelKey='label' valueKey='value' />
                    </FormGroup>
                    <FormGroup style={{marginTop:10}}>
                        <ControlLabel>Email: </ControlLabel>
                        <Input type='email' value={email} onChange={(v)=>{setEmail(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginTop:10}}>
                        <ControlLabel>Phone Number: </ControlLabel>
                        <Input  type='tel' value={phone} onChange={(v)=>{setPhone(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginTop:10}}>
                        <ControlLabel>Note: </ControlLabel>
                        <Input value={note} onChange={(v)=>{setNote(v)}}  />
                    </FormGroup>
                    <FormGroup style={{marginTop:10}}>
                       <Button  onClick={()=>{backStep() }}>Back</Button> <Button onClick={ContinueReview} disabled={tname==='' || gender===''|| email===''|| phone==='' || note==='' ? true:false} appearance='primary'>Continue</Button>
                    </FormGroup>
                </Col>
            </Row>
        </div>
    )
}

export default Traveler