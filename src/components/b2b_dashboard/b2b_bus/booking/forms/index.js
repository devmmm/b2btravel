import React from 'react'
import Seats from './Seats'
import Traveler from './Traveler'
import Review from './Review'

function BookingFrom(props) {
  let  tripInfo=props.tripInfo 
  let not=props.not 
  let step=props.step
  let setStep=props.setStep
  let nationality=props.nationality;
    return (
        <div>
           {
               step === 0 ? <Seats tripInfo={tripInfo} leavingfrom={props.leavingfrom} goingto={props.goingto}  not={not} setStep={setStep} nationality={nationality} />:null
           }
           { 
               step=== 1 ? <Traveler not={not} setStep={setStep} />:null
           }
           {
               step===2 ? <Review setStep={setStep} />:null
           }
        </div>
    )
}

export default BookingFrom
