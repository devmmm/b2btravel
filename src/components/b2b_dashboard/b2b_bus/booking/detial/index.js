import React, { useState, useEffect } from 'react'
import { b2b_user_data } from '../../../../../tools/constants';
import { check_token, refresh_token } from '../../../../../tools/helpers';
import { BUS_API } from '../../../../../tools/api';
import { Divider, Form, Row, Grid, Placeholder, InputGroup, ControlLabel, Input,Col, FormGroup, Button, Panel, Icon } from 'rsuite';
import { useHistory } from 'react-router-dom';


function BookingDetial(props) {
    let booking_id = props.match.params.id;
    const [booking_detial, setBooking_detial] = useState(null)
    const [loading, setLoading] = useState(false)
    const history = useHistory()
    useEffect(() => {
        setLoading(true)
      if(b2b_user_data.access_token!==null){
        if(check_token(b2b_user_data.create_time)){
          BUS_API(b2b_user_data.access_token).get(`/bookingdetail?bookingid=${booking_id}`)
          .then(res=>{
              console.log(res.data)
              setLoading(false)
              if(res.data.Data!=='None'){
                setBooking_detial(res.data.Data)
            }
          })
          .catch(err=>{
              console.log(err)
              setLoading(false)
          })
        }
        else{
            if(refresh_token(b2b_user_data.refresh_token_old)){
                BUS_API(b2b_user_data.access_token).get(`/bookingdetail?bookingid=${booking_id}`)
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    if(res.data.Data!=='None'){
                        setBooking_detial(res.data.Data)
                    }
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })
            }
        }
      }
    }, [b2b_user_data.create_time])
    return (
        <div>
           <Divider>BUS BOOKING DETIAL</Divider>
           <Grid >
           <Row>
              {
                  loading ?
                  <Col xs={24} sm={24} md={24} lg={24}>
                  <div className='bus_booking_detial_loader'>
                     <Icon spin icon='spinner' size='2x' />
                  </div>
                  
                  </Col>:
                   <Col xs={24} sm={24} md={24} lg={24}>
                     {
                        booking_detial!==null ? 
                       
                       <Panel className='bus_booking_detial_div' header={<p className='bus_booking_heading'> Reference- {booking_id} </p>} >
                          <Row>
                             <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='detial_name'>
                                 Bus Operator
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                                 <p className='detial_name'>
                                  Bus Type 
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.operatorcode} 
                             </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                            <p className='detial_value'>
                                
                            </p>
                           </Col>
                          </Row>
                          <Divider />
                          <Row>
                             <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='detial_name'>
                               From
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                                 <p className='detial_name'>
                              To
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.depfrom} 
                             </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                            <p className='detial_value'>
                            {booking_detial.arrvto} 
                            </p>
                           </Col>
                          </Row>
                          <Row>
                             <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='detial_name'>
                             Depatrue Date
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                                 <p className='detial_name'>
                             Arrival Time
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.depdate} , {booking_detial.deptime}
                             </p>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24}>
                            <p className='detial_name'>
                             Route
                            </p>
                           </Col>
                           <Col xs={24} sm={24} md={24} lg={24}>
                            <p className='detial_value'>
                            From  {booking_detial.depfrom}  To  {booking_detial.arrvto} 
                            </p>
                           </Col>
                          </Row>
                          <Divider />
                          <Row>
                             <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='detial_name'>
                            Traveller Name
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                                 <p className='detial_name'>
                             Seat Number
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.pax_name}
                             </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.seats} 
                             </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='detial_name'>
                              Phone Number
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                                 <p className='detial_name'>
                            Booking Date
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.pax_contactno}
                             </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {new Date(booking_detial.bookDateTime).toLocaleString()}
                             </p>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24}>
                            <p className='detial_name'>
                            Special Request
                            </p>
                           </Col>
                           <Col xs={24} sm={24} md={24} lg={24}>
                            <p className='detial_value'>
                           
                            </p>
                           </Col>
                          </Row>
                          <Divider />
                          <Row>
                             <Col xs={12} sm={12} md={12} lg={12}>
                               <p className='detial_name'>
                                Number of Seats
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                                 <p className='detial_name'>
                                Total 
                               </p>
                             </Col>
                             <Col xs={12} sm={12} md={12} lg={12}>
                             <p className='detial_value'>
                                 {booking_detial.pax_no} 
                             </p>
                            </Col>
                            <Col xs={12} sm={12} md={12} lg={12}>
                            <p className='detial_value'>
                            {booking_detial.perpaxprice}  {booking_detial.perpaxpricecurrency} 
                            </p>
                           </Col>
                           <Col xs={24} sm={24} md={24} lg={24}>
                            <p className='detial_name'>
                            Booking Status
                            </p>
                           </Col>
                           <Col xs={24} sm={24} md={24} lg={24}>
                            <p className='detial_value'>
                            {booking_detial.bookingStatus} 
                            </p>
                           </Col>
                          </Row>
                       </Panel >:<div style={{display:'flex',justifyContent:'center',alignContent:'center',alignItems:'center',minHeight:400}}>
                      <p> No Data! </p>
                     </div>
                     }
                     <pre>{JSON.stringify(booking_detial,null,2)} </pre>
                   </Col>
              }
           </Row>
           </Grid>
           
        </div>
    )
}

export default BookingDetial
