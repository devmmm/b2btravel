import React from 'react'
import { Panel, Icon } from 'rsuite'

function TripInfo(props) {
    return (
        <div style={{marginTop:10,marginBottom:10}}>
           <Panel style={{minHeight:150}}>
                    <Panel style={{background:'#f2f2f2'}} header={props.tripInfo.operatorNameEN} >
                    <p><Icon icon='bus' /> {props.tripInfo.sourceNameEN} ({props.tripInfo.sourceNameMM}) <Icon icon='long-arrow-right'  />  <Icon icon='bus' /> {props.tripInfo.destinationNameEN} ({props.tripInfo.destinationNameMM})  </p>
                    <p><small> <Icon icon='street-view' /> {props.tripInfo.gateNameEN} </small>  </p>
                    <p> <Icon icon='openid' /> <small>{props.tripInfo.busTypeNameEN} </small> </p>
                    <p><Icon icon='money' /> {props.tripInfo.localTicketPrice.amount}{props.tripInfo.localTicketPrice.currency} </p>
                    <small> <Icon icon='user' /> {props.not} person{parseInt(props.not)>1 ?'s':''} </small>
                    </Panel>
                   
                </Panel>
        </div>
    )
}

export default TripInfo
