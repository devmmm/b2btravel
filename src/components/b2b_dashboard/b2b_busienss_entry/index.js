import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Profile from './profile'
import Balance from './balance'
import Exchange from './exchange'
import Transactions from './transactions'

function B2BBusienss_Entry(props) {
    return (
        <div>
            <Switch>
              <Route exact path={`${props.route}`}  component={()=>{return(<Profile userData={props.userData} />)}} />
              <Route exact path={`${props.route}/balance`}  component={()=>{return(<Balance/>)}} />
              <Route exact path={`${props.route}/exchange`}  component={Exchange} />
              <Route exact path={`${props.route}/transactions`}  component={Transactions} />
            </Switch>
        </div>
    )
}

export default B2BBusienss_Entry
