import React, { useState, useEffect } from 'react'
import { check_token, refresh_token } from '../../../../tools/helpers';
import { BE_API } from '../../../../tools/api';
import { Divider, Grid, Row, Col, Panel, Icon, FormGroup, Button, Placeholder } from 'rsuite';

function Balance() {
    const access_token = localStorage.getItem('access_token');
    const create_time = localStorage.getItem('create_time');
    const refresh_token_old = localStorage.getItem('refresh_token')
    const [businessBalance, setBusinessBalance] = useState([])
    const [loading, setLoading] = useState(false)
    const [reload, setReload] = useState(false)
    useEffect(() => {
     if(access_token!==null){
         setLoading(true)
         if(check_token(create_time)){
          BE_API(access_token).get('/user/getbalance/')
          .then(res=>{
              console.log(res.data)
              setLoading(false)
              setBusinessBalance(res.data.Data.businessBalance)
          })
          .catch(err=>{
              console.log(err)
              setLoading(false)
          })
         }
         else{
             if(refresh_token(refresh_token_old)){
                BE_API(access_token).get('/user/getbalance/')
                .then(res=>{
                    console.log(res.data)
                    setLoading(false)
                    setBusinessBalance(res.data.Data.businessBalance)
                })
                .catch(err=>{
                    console.log(err)
                    setLoading(false)
                })
             }
         }
     }
    }, [create_time,reload])
    return (
        <div>
            <Divider>BALANCE</Divider>
             {
                 loading ? 
                 <Grid>
                 <Row>
                 <Col  xs={24} sm={24} md={12} lg={12}>
                            <Placeholder.Graph active height={200} />
                </Col>
                <Col  xs={24} sm={24} md={12} lg={12}>
                            <Placeholder.Graph  active height={200} />
                </Col>
                 </Row>
             </Grid>:
             <Grid>
             <Row>
            
                {
                    businessBalance.map((b,i)=>(
                        <Col key={i} xs={24} sm={24} md={12} lg={12}>
                        <Panel style={{background:'#f2f2f2',marginTop:10}}>
                          <div style={{textAlign:'center'}}>
                             <Icon icon={b.currency==='MMK' ? 'money':'usd'} size='4x' />
                             <p><Icon icon='user' /> {b.accountID}</p>
                             <p style={{color:'teal'}}>{b.currency} </p>
                             <p style={{color:'red'}}>{b.balance} </p>
                             <p><small>{new Date(b.updateDate).toLocaleString()} </small> </p>
                          </div>
                        </Panel>
                      </Col>
                    ))
                }
                <Col  xs={24} sm={24} md={24} lg={24}>
                   <FormGroup style={{marginTop:10,marginBottom:10}}>
                   <Button   appearance='primary' color='green'> add </Button>    <Button appearance='subtle' onClick={()=>{setReload(!reload)}}> <Icon  icon='refresh' /> </Button>    
                   </FormGroup>
                        
              </Col>
             </Row>
         </Grid>
             }
           
        </div>
    )
}

export default Balance
