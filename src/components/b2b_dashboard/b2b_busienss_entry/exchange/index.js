import React, { useState, useEffect } from 'react'
import { Divider, Grid, Col, Panel, Form, RadioGroup, Radio, Icon, InputGroup, Input, Button, FormGroup, ControlLabel, Message, Alert } from 'rsuite'
import { check_token, refresh_token } from '../../../../tools/helpers'
import { BE_API } from '../../../../tools/api'

function Exchange() {
    const [action, setAction] = useState('buy')
    const [amount, setamount] = useState('100')
    const [rates, setRates] = useState([])
    const access_token = localStorage.getItem('access_token')
    const refresh_token_old = localStorage.getItem('refresh_token')
    const create_time = localStorage.getItem('create_time')
    const [loading, setloading] = useState(false)
    const [step, setStep] = useState(0)
    useEffect(() => {
       if(access_token!==null){
           if(check_token(create_time)){
               BE_API(access_token).get('/user/exchangerate/')
               .then(res=>{
                   console.log(res.data)
                   setRates(res.data.Data.rates)
               })
               .catch(err=>{
                   console.log(err)
               })
           }
           else{
            if( refresh_token(refresh_token_old)) {
                
                BE_API(localStorage.getItem('access_token')).get('/user/exchangerate/')
                .then(res=>{
                  console.log(res.data)
                  setRates(res.data.Data.rates)
                })
                .catch(error=>{
                  console.log(error)
                })
               }
           }
       } 
    }, [create_time])

    const comFirmExchange = () =>{
        var data = {
            "currfrom":"USD",
            "currto":"MMK",
            "action":action,
            "amount":amount
        }
        if(access_token!==null){
            if(check_token(create_time)){
                setloading(true)
                BE_API(access_token).post('/user/exchange/',data)
                .then(res=>{
                    setloading(false)
                    console.log(res.data)
                    setStep(0)
                    setamount('0')
                    if(res.data.Data==='OK'){
                        Alert.success('Exchnage Successful! check your balance again!')

                    }
                    else{
                        Alert.error(res.data.Data)
                    }
                })
                .catch(err=>{
                    setloading(false)
                    console.log(err)
                    setStep(0)
                    setamount('0')
                })
            }
            else{
                if(refresh_token(refresh_token_old)){
                    BE_API(localStorage.getItem('access_token')).post('/user/exchange/',data)
                    .then(res=>{
                        setloading(false)
                        console.log(res.data)
                        setStep(0)
                        setamount('0')
                    })
                    .catch(err=>{
                        setloading(false)
                        console.log(err)
                        setStep(0)
                        setamount('0')
                })
                }
            }
        }
    }
    return (
        <div>
            <Divider>EXCHANGE</Divider>
            <Grid>
              {
                  step===0 ?
                  <>
                  <Col xs={24} md={24} lg={24} sm={24}>
                       <Panel style={{background:'#f2f2f2',minHeight:140}} >
                       <Form >
                           <Grid>
                               <Col xs={24} sm={24} md={8} lg={8} >
                                   <FormGroup>
                                       <ControlLabel> Choose Action </ControlLabel>
                                       <RadioGroup value={action} style={{marginBottom:10}} onChange={(v)=>{setAction(v)}}  inline >
                                       <Radio value='sell' >
                                           USD <Icon icon='arrow-circle-o-right' /> MMK
                                       </Radio>
                                       <Radio value='buy' >
                                           MMK <Icon icon='arrow-circle-o-right' /> USD
                                       </Radio>
                                   </RadioGroup>
                                   </FormGroup> 
                               </Col>
                               <Col xs={24} sm={24} md={10} lg={10} >
                               <FormGroup>
                                   <ControlLabel>Enter USD </ControlLabel>
                                   <InputGroup   style={{marginBottom:10}}>
                                   <InputGroup.Addon><Icon icon='usd' /> </InputGroup.Addon>
                                   <Input value={amount} onChange={(v)=>{setamount(v)}} />
                                </InputGroup>
                               </FormGroup>
                               </Col>
                               <Col xs={24} sm={24} md={6} lg={6} >
                                <FormGroup>
                                    <Button onClick={()=>{setStep(1)}} disabled={amount>0 ? false:true} appearance='primary'  size='md'  style={{marginBottom:10,marginTop:25}} > Convert </Button>
                                </FormGroup>
                               </Col>
                           </Grid>
                           </Form>
                       </Panel>
                       <div style={{marginTop:20,marginBottom:20}}>
                          {
                              amount>0?
                              <Message type='error' title='Comfimation' description={
                               <>
                               {
                                   rates.map((r,i)=>(
                                       <div key={i}>
                                          {
                                            action==='sell' ? 
                                            <p>
                                                {(parseInt(r.buy)*amount)} MMK will be added to your MMK account and {amount} USD will be reduced from your USD account.
                                           </p>:
                                            <p>
                                                {(parseInt(r.sell)* amount)} MMK  will be reduced from your MMK account and {amount} USD will be added to your USD account.
                                            </p>
                                          }
                                         
                                       </div>
                                   ))
                               }
                               </> 
                            } />:null
                          }

                       </div>
                   </Col>
                  </>:null
              }
              {
                  step===1 ?
                  <>
                    <Col sm={24} md={24} lg={24} xs={24}>
                    <div style={{marginTop:20,marginBottom:20}}>
                    <Message type='error' title='Comfimation' description={
                               <>
                               {
                                   rates.map((r,i)=>(
                                       <div key={i}>
                                          {
                                            action==='sell' ? 
                                            <p>
                                                {(parseInt(r.buy)*amount)} MMK will be added to your MMK account and {amount} USD will be reduced from your USD account.
                                           </p>:
                                            <p>
                                                {(parseInt(r.sell)* amount)} MMK  will be reduced from your MMK account and {amount} USD will be added to your USD account.
                                            </p>
                                            
                                          }
                                           
                                       </div>
                                   ))
                               }
                               </> 
                            } /> 
                            <div style={{marginTop:10,marginBottom:10}}>
                            <Button onClick={()=>{setStep(0)}} appearance='default'>Cancel</Button> <Button loading={loading} onClick={()=>{comFirmExchange()}} appearance='primary'>Comfirm</Button>
                            </div>
                    </div>
                    </Col>
                  </>:null
              }
            </Grid>
        </div>
    )
}

export default Exchange