import React, { useState, useEffect } from 'react'
import { Divider, Grid, Col,Table, DateRangePicker, Icon, Button, RadioGroup, Radio, Placeholder, ControlLabel } from 'rsuite'
import { check_token, refresh_token } from '../../../../tools/helpers'
import { BE_API } from '../../../../tools/api'

function Transactions() {
  const {Column,HeaderCell,Cell} = Table
  const [transactions, setTransactions] = useState([])
  const [selectDate,setselectDate] = useState([new Date('2020-05-06'),new Date('2020-05-30')])
  const [selectAccount, setSelectAccount] = useState('')
  const access_token=localStorage.getItem('access_token')
  const refresh_token_old=localStorage.getItem('refresh_token')
  const create_time = localStorage.getItem('create_time')
  const [loading, setLoading] = useState(false)
  const [reload, setreload] = useState(false)
  const [accounts, setAccounts] = useState([])
  useEffect(() => {
   if(access_token!==null){
     if(check_token(create_time)){
       setLoading(true)
       BE_API(access_token).get('/user/getbalance/')
       .then(res=>{
         console.log(res.data)
         setAccounts(res.data.Data.businessBalance)
         setLoading(false)
       })
       .catch(err=>{
         console.log(err)
         setLoading(false)
       })
     }
     else{
       if(refresh_token(refresh_token_old)){
         setLoading(true)
        BE_API(localStorage.getItem('access_token')).get('/user/getbalance/')
        setLoading(false)
        .then(res=>{
          console.log(res.data)
          setAccounts(res.data.Data.businessBalance)
        })
        .catch(err=>{
          setLoading(false)
          console.log(err)
        })
       }
     }
   }
  }, [create_time])

  useEffect(() => {
    var data = {
      "fromdate":selectDate[0],
      "todate": selectDate[1],
      "accountid":selectAccount
  } 
  if(selectAccount!=='') {
    if(access_token!==null){
      if(check_token(create_time)){
        setLoading(true)
        BE_API(access_token).post('/user/transactions/',data)
        .then(res=>{
          console.log(res.data)
          setTransactions(res.data.Data)
          setLoading(false)
        })
        .catch(err=>{
          console.log(err)
          setLoading(false)
        })
      }
      else{
        if(refresh_token(refresh_token_old)){
          setLoading(true)
         BE_API(localStorage.getItem('access_token')).post('/user/transactions/',data)
         .then(res=>{
           console.log(res.data)
           setAccounts(res.data.Data)
           setLoading(false)
         })
         .catch(err=>{
           console.log(err)
           setLoading(false)
         })
        }
      }
    }
  } 
    
   }, [selectDate,selectAccount,reload])
    return (
        <div>
          <Divider style={{fontWeight:'bold',textTransform:'uppercase'}}>Transactions</Divider> 
          <Grid fluid>
            <Col xs={24} sm={24} md={8} lg={8} >
             <div style={{marginTop:10,marginBottom:10}}>
             <ControlLabel>Choose Date </ControlLabel>
              <DateRangePicker value={selectDate}   onChange={(val)=>{setselectDate(val)}}  appearance='subtle' />
             </div>
            </Col>
            <Col xs={24} sm={24} md={8} lg={8} >
              <div style={{marginBottom:10,marginTop:10}}>
              <ControlLabel>Account</ControlLabel>
              {
                  loading ?
                  <Placeholder.Graph height={30} active />:
                  <RadioGroup appearance='default' inline value={selectAccount} onChange={(v)=>{setSelectAccount(v)}} >
               {
                accounts.map((a,i)=>(
                  <Radio key={i} value={a.accountID} >{a.accountID} </Radio>
                ))
               }
             </RadioGroup>
              }
              </div>
            </Col>
            
            <Col sm={24} sm={24} md={24} lg={24}>
             
               
                 <Table
                 loading={loading}
                 height={300}
                 data={transactions}
               >
                
       
                 <Column width={300} fixed>
                   <HeaderCell>Ref No. </HeaderCell>
                   <Cell dataKey="refNo" />
                 </Column>
       
                 <Column width={100}>
                   <HeaderCell>Type</HeaderCell>
                   <Cell dataKey="transactionType" />
                 </Column>
       
                 <Column width={150}>
                   <HeaderCell>Amount </HeaderCell>
                   <Cell dataKey="amount" />
                 </Column>
       
                 <Column width={150}>
                   <HeaderCell>Balance</HeaderCell>
                   <Cell dataKey="balance" />
                 </Column>
       
                 <Column width={300}>
                   <HeaderCell>Description</HeaderCell>
                   <Cell dataKey="description" />
                 </Column>
       
                 <Column width={200} align='center'  >
                                   <HeaderCell>CreateDate </HeaderCell>
                                   <Cell>
                                     {rowData => {
                                       
                                       return (
                                         <span>
                                            {new Date(rowData.createDate).toLocaleString()}
                                         </span>
                                       );
                                     }}
                                   </Cell>
                                 </Column>
                                 <Column width={100}>
                   <HeaderCell>User</HeaderCell>
                   <Cell dataKey="createdBy" />
                 </Column>
               </Table>
              
            </Col>
          </Grid>
        </div>
    )
}

export default Transactions