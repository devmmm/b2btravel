import React, { useState } from 'react'
import { Divider, Grid, Row, Col, Avatar, FormGroup, ControlLabel, Input, Button } from 'rsuite'

function Profile(props) {
    const [username, setUsername] = useState(props.userData.userName)
    const [userEmail, setUserEmail] = useState(props.userData.userEmail)
    const [businessName, setBusinessName] = useState(props.userData.businessInfo.businessName)
    const [businessPhone, setBusinessPhone] = useState(props.userData.businessInfo.businessPhone)
    const [businessAddress, setbusinessAddress] = useState(props.userData.businessInfo.businessAddress)
    const [businessCity, setBusinessCity] = useState(props.userData.businessInfo.businessCity)
    const [businessCountry, setBusinessCountry] = useState(props.userData.businessInfo.businessCountry)

    return (
        <div>
           <Divider>USER INFORMATION</Divider>
           <Grid>
               <Row>
               <Col xs={24} sm={24} md={18} lg={18}>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>User Name</ControlLabel>
                        <Input disabled value={username} onChange={(v)=>{setUsername(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>Email Address</ControlLabel>
                        <Input disabled value={userEmail} onChange={(v)=>{setUserEmail(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>Business Name</ControlLabel>
                        <Input disabled value={businessName} onChange={(v)=>{setBusinessName(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>Business Phone</ControlLabel>
                        <Input disabled value={businessPhone} onChange={(v)=>{setBusinessPhone(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>Business Address</ControlLabel>
                        <Input disabled value={businessAddress} onChange={(v)=>{setbusinessAddress(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>Business City</ControlLabel>
                        <Input disabled value={businessCity} onChange={(v)=>{setBusinessCity(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                        <ControlLabel>Business Country</ControlLabel>
                        <Input disabled value={businessCountry} onChange={(v)=>{setBusinessCountry(v)}} />
                    </FormGroup>
                    <FormGroup style={{marginBottom:10}}>
                       <Button appearance='primary' color='green' >update</Button>
                    </FormGroup>
                   </Col>
                   <Col xs={24} sm={24} md={6} lg={6}>
                    <Avatar style={{marginLeft:'auto',display:'flex'}} alt={props.userData.userName} circle size='lg' src={props.userData.businessInfo.businessLogo} />
                   </Col>
                  
               </Row>
           </Grid>
        </div>
    )
}

export default Profile
