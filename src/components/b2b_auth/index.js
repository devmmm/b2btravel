import React, { useState } from "react";
import LoginForm from "./login_form";
import Logo from "../../assets/logo.png";
import { Animation, Notification, Icon } from "rsuite";
import { LOGIN_API } from "../../tools/api/";
import { useHistory } from "react-router-dom";
import { theme } from "../../tools/theme";
function B2BAuth() {
  const { Bounce } = Animation;
  const [business_id, setBusiness_id] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const B2B_Login = () => {
    var form_data = new FormData();
    setLoading(true);
    form_data.append("businessentity", business_id);
    form_data.append("username", username);
    form_data.append("password", password);
    LOGIN_API()
      .post("/token-auth/", form_data)
      .then((res) => {
        setLoading(false);
        localStorage.setItem("access_token", res.data.access);
        localStorage.setItem("refresh_token", res.data.refresh);
        localStorage.setItem("create_time", new Date().getTime());
        history.push("/dashboard");
      })
      .catch((err) => {
        console.log(err);
        Notification.error({
          title: "B2B.TRAVELS.COM.MM",
          description: "Something Worng.Please try again!",
          top: 1,
          style: {
            fontFamily: theme.default.fontfamily,
          },
        });
        setLoading(false);
      });
  };
  return (
    <div
      style={{
        height: "100vh",
        background: "#fff",
        display: "flex",
        justifyContent: "center",
        alignContent: "center",
        justifyItems: "center",
        alignItems: "center",
      }}
    >
      <Bounce in={true} animation timeout={10000}>
        {loading ? (
          <div
            style={{
              minWidth: 350,
              minHeight: 200,
              padding: 15,
              borderRadius: 5,
              textAlign: "center",
            }}
          >
            <Icon spin size="5x" icon="spinner" />
          </div>
        ) : (
          <div
            style={{
              minWidth: 350,
              minHeight: 200,
              background: "#f2f2f2",
              padding: 15,
              borderRadius: 5,
              textAlign: "center",
            }}
          >
            <img src={Logo} alt="logo" width={50} height={50} />
            <LoginForm
              B2B_Login={B2B_Login}
              business_id={business_id}
              setBusiness_id={setBusiness_id}
              username={username}
              setUsername={setUsername}
              password={password}
              setPassword={setPassword}
            />
          </div>
        )}
      </Bounce>
    </div>
  );
}

export default B2BAuth;
