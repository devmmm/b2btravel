import React,{useState} from 'react'
import { Form, InputGroup, Icon, Input, FormGroup, Button } from 'rsuite'
 
function LoginForm(props) {
    const business_id = props.business_id;
    const setBusiness_id=props.setBusiness_id;
    const username=props.username;
    const setUsername= props.setUsername;
    const password= props.password;
    const setPassword=props.setPassword;
    const B2B_Login = props.B2B_Login;
    const [show, setShow] = useState(false)
    return (
        <div>
           <Form fluid>
            <InputGroup  style={{marginTop:10,marginBottom:10}}>
              <InputGroup.Addon> <Icon icon='industry' /> </InputGroup.Addon>
              <Input  value={business_id} onChange={(v)=>{setBusiness_id(v)}} />
            </InputGroup>
            <InputGroup style={{marginTop:10,marginBottom:10}}>
              <InputGroup.Addon> <Icon icon='user' /> </InputGroup.Addon>
              <Input value={username} onChange={(v)=>{setUsername(v)}} />
            </InputGroup>
            <InputGroup style={{marginTop:10,marginBottom:10}}>
              <InputGroup.Addon> <Icon icon='key' /> </InputGroup.Addon>
              <Input type={show ? 'text':'password'} value={password} onChange={(v)=>{setPassword(v)}} />
              <InputGroup.Button onClick={()=>{setShow(!show)}}> <Icon icon={show ? 'eye':'eye-slash'} /> </InputGroup.Button>
            </InputGroup>
            <FormGroup style={{marginTop:10,marginBottom:10}}>
             <Button  onClick={()=>{B2B_Login()}} disabled={business_id===''|| username==='' || password==='' ? true:false} appearance='primary' color='cyan' block>  LOGIN </Button>
             <Button   appearance='link' color='red' block>  Forgot your password? </Button>
            </FormGroup>
           </Form>
        </div>
    )
}

export default LoginForm
