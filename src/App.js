import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import B2BAuth from './components/b2b_auth'
import B2BDashboard from './components/b2b_dashboard/dashboard'
import PageNotFound from './components/b2b_404'
import {Provider as StoreProvider} from 'react-redux'
import store from './tools/storage/root/store'
import {theme} from './tools/theme/'
import './App.css';
function App() {
  return (
   <StoreProvider store={store}>
      <div style={{fontFamily:theme.default.fontfamily}}>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={B2BAuth} />
          <Route path='/dashboard' component={B2BDashboard} />
          <Route path='/404' component={PageNotFound} />
          <Redirect to='/404' />
        </Switch>
      </BrowserRouter>
    </div>
   </StoreProvider>
  )
}

export default App
