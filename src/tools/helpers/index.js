import { BE_API } from "../api";

export function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');


}


export function check_token(create_time){
    let current_time = new Date().getTime()
    let dif_time = current_time - create_time
    if((dif_time/(60*1000))<45){
        return true
    }
    else {
        return false
    }
}

export function refresh_token(refresh_token){
    var form = new FormData()
    form.append('refresh',refresh_token)
    BE_API(refresh_token).post('/admin-token-auth/refresh',form)
    .then(res=>{
        console.log(res.data)
        localStorage.setItem('access_token',res.data.access)
        localStorage.setItem('create_time',new Date().getTime())
        window.location.reload()
        return true
    })
    .catch(err=>{
        console.log('session_timeout!','Timeout!')
        return false
    })
}

export function timeConvert(n) {
    var num = n;
    var hours = (num * 2.7777777777778E-7);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return  rhours + " hour(s) and " + rminutes + " minute(s).";
    }

    export function ValidateEmail(mail) 
    {
     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
      {
        return (true)
      }
    
        return (false)
    }
    
    export function Validatephonenumber(number)
    {
     var pattern = new RegExp(/^[0-9\b]+$/);
     if(!pattern.test(number)){
         return false;
     }
     else {
         return true
     }
    }