import {createStore,combineReducers} from 'redux'
import BusReducer from '../bus/Reducer'
import AuthReducer from '../auth/Reducer'
import BusBookingReducer from '../bus/booking/Reducer'
import FlightReducer from '../flight/Reducer'
import FlightBookStepReducer from '../flight/booking_steps/Reducer'
const RootReduer = combineReducers({
    auth:AuthReducer,
    bus:BusReducer,
    bus_booking:BusBookingReducer,
    flight:FlightReducer,
    flight_book_step:FlightBookStepReducer
})
const store = createStore(RootReduer)
export default store