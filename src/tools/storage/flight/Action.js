import {TRAVELLER_INFO_FLIGHT,CONTACT_INFO,PAYMENT_INFO,FLIGHT_INFO,REFEACTOR} from './Type'

export function flight_info(id,fsc,fs,amount,currencycode){
    return{
        type:FLIGHT_INFO,
        payload:{
         id,
         fsc,
         fs,
         amount,
         currencycode
        }
    }
}
export function traveller_info_flight(travellers){
 return{
     type:TRAVELLER_INFO_FLIGHT,
     payload:{
         travellers
     }
 }
}

export function contact_info(contacts){
    return{
        type:CONTACT_INFO,
        payload:{
            contacts
        }
    }
   }

   export function payment_info(paymentType,paymentDescription){
    return{
        type:PAYMENT_INFO,
        payload:{
            paymentType,
            paymentDescription
        }
    }
   }

   export function refactor_booking(){
       return{
           type:REFEACTOR
       }
   }
