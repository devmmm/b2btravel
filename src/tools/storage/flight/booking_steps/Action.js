import {NEXT,PREVIOUS,RESET} from './Type'

export  function next_flight_book(){
    return {
        type:NEXT
    }
}

export  function previous_flight_book(){
    return{
        type:PREVIOUS
    }
}
export  function reset_flight_book(){
    return{
        type:RESET
    }
}