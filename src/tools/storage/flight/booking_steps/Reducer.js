import {NEXT,PREVIOUS,RESET} from './Type'
const initialState = {
booking:{
    step:0
}
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case NEXT:
        return { ...state, 
         booking:{
             ...state.booking,
             step:state.booking.step+1
         }
        }
    case PREVIOUS:
        return { ...state, 
            booking:{
                ...state.booking,
                step:state.booking.step-1
            }
        }
        case PREVIOUS:
            return initialState
            
    default:
        return state
    }
}
