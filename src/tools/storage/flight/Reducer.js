import {CONTACT_INFO,TRAVELLER_INFO_FLIGHT,PAYMENT_INFO, FLIGHT_INFO,REFEACTOR} from './Type'
import { formatDate } from '../../helpers'


const initialState = {
 booking:{
    searchID:"",
    fsc:"",
    src:"",
    travelers:null,
    contacts:{
        userID:"",
        contactName:{
            firstName: "",
            lastName: ""
        },
        phone:{
            countryCallingCode:"",
            contactNumber:""
        },
        emailAddress: ""
    },
  payment:{
    paymentType:"Deposit",
    totalAmount:"",
    currencyCode:"",
    paymentDesc:"Agent Deposit"
  }
}
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case FLIGHT_INFO:
        return { ...state, 
         booking:{
             ...state.booking,
             searchID:payload.id,
             fsc:payload.fsc,
             src:payload.fs,
             payment:{
                 ...state.booking.payment,
                 totalAmount:payload.amount,
                 currencyCode:payload.currencycode
             }

         }
        }

        case TRAVELLER_INFO_FLIGHT:
            return { ...state, 
            
             booking:{
                ...state.booking,
                 travelers:payload.travellers
             }
             
            }
        case CONTACT_INFO:
            return { ...state, 
                booking:{
                    ...state.booking,
                    contacts:payload.contacts
                    }    
                
            }
        case PAYMENT_INFO:
            return { ...state, 
                booking:{
                    ...state.booking,
                    payment:{
                        ...state.booking.payment,
                        paymentType: payload.paymentType,
                        paymentDesc:payload.paymentDescription
                    }
                    }    
                
            }
        case REFEACTOR:
            return{
                ...state,
                booking:{
                    ...state.booking,
                    travelers:{
                        ...state.booking.travelers,
                        dateOfBirth:formatDate(state.booking.travelers.dateOfBirth),
                        passengerDocuments:{
                            ...state.booking.travelers.passengerDocuments,
                            documentExpireDate:formatDate(state.booking.travelers.passengerDocuments.documentExpireDate)
                        }
                    }
                }
            }
    

    default:
        return state
    }
}
