import {TRIP_INFO,TRAVELLER_INFO,SEAT_SELECTION} from './Type'

const initialState = {
booking_data:{
    pax_name:"",
    pax_contactno: "",
    pax_email: "",
    pax_gender: "",
    pax_ID: "",
    pax_remark: "",
    pax_no:1,
    operatorcode: "",
    seatPlanUrl:"",
    tripID:"",
    depfrom:"" ,
    arrvto:"" ,
    depdate:"",
    deptime: "",
    nationality: "MM",
    seats:[
        1
    ],
    perpaxprice:{
        amount:"",
        currency:"MMK"
    }
}
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case TRIP_INFO:
        return { ...state, 
         booking_data:{
             ...state.booking_data,
            
                 operatorcode:payload.trip_info.operatorcode,
                 seatPlanUrl:payload.trip_info.seatPlanUrl,
                 tripID:payload.trip_info.tripID,
                 depfrom:payload.trip_info.depfrom,
                 arrvto:payload.trip_info.arrvto,
                 depdate:payload.trip_info.depdate,
                 deptime:payload.trip_info.deptime,
                 nationality: payload.trip_info.nationality,
                 perpaxprice:payload.trip_info.perpaxprice
           
         }
        }
    case TRAVELLER_INFO:{
        return{
            ...state,
            booking_data:{
                ...state.booking_data,
                pax_name:payload.traveller_info.pax_name,
                pax_contactno:payload.traveller_info.pax_contactno,
                pax_email:payload.traveller_info.pax_email,
                pax_gender:payload.traveller_info.pax_gender,
                pax_ID: payload.traveller_info.pax_ID,
                pax_remark: payload.traveller_info.pax_remark,
            }
        }
    }
    case SEAT_SELECTION:{
        return{
            ...state,
            booking_data:{
                ...state.booking_data,
               
                    seats:payload.seat
               
            }
        }
    }

    default:
        return state
    }
}
