import {TRAVELLER_INFO,SEAT_SELECTION,TRIP_INFO} from './Type'

export function seat_selection(seat){
  return{
      type:SEAT_SELECTION,
      payload:{
          seat
      }
  }
}

export function traveller_info(traveller_info){
   return{
    type:TRAVELLER_INFO,
    payload:{
        traveller_info
    }
   }
}

export function trip_info(trip_info){
    return{
        type:TRIP_INFO,
        payload:{
            trip_info
        }
    }
}