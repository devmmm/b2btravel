import {NEXT,BACK,TRAVELER_INFO,PAYMENT,COMFIRMATION,SEAT_SELECTION} from './Type'
const initialState = {
booking:{
    step:0
}
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case NEXT:
        return { ...state, 
         booking:{
             ...state.booking,
             step:state.booking.step+1
         }
        }
    case BACK:
        return { ...state, 
            booking:{
                ...state.booking,
                step:state.booking.step-1
            }
        }

    default:
        return state
    }
}
