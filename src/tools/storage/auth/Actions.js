import {LOGIN,LOGOUT,REFRESH_TOKEN} from './Type'

export function auth_login(access_token,refresh_token,create_time) {
    return {
        type:LOGIN,
        payload:{
            access_token,
            refresh_token,
            create_time
        }
    }
}

export function auth_logout(){
    return {
        type:LOGOUT
    }
}

export function auth_token_refresh(access_token,create_time){
return{
    type:REFRESH_TOKEN,
    payload:{
        access_token,
        create_time
    }
}
}