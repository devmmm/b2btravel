import {LOGOUT,LOGIN,REFRESH_TOKEN} from './Type'

const initialState = {
token :{
    access_token:null,
    refresh_token:null,
    create_time:null,
    status:false
}
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case LOGIN:
        return { ...state,
        token:{
            access_token:payload.access_token,
            refresh_token:payload.refresh_token,
            create_time:payload.create_time,
            status:true
        }
        }
    case REFRESH_TOKEN:
        return{
            ...state,
            token:{
                ...state.token,
                access_token:payload.access_token,
                create_time:payload.create_time
            }
        }
    case LOGOUT:
        return initialState;
    default:
        return state
    }
}
