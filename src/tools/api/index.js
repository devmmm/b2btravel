import axios from "axios";
import { b2b_variables } from "../constants/";

export const LOGIN_API = () =>
  axios.create({
    baseURL: b2b_variables.base_url.business_entry,
    headers: {
      "Content-Type": "application/json",
    },
    responseType: "json",
  });

export const BE_API = (access_token) =>
  axios.create({
    baseURL: b2b_variables.base_url.business_entry,
    headers: {
      "Content-Type": "application/json",
      Authorization: "JWT " + access_token,
    },
    responseType: "json",
  });

export const DS_API = (access_token) =>
  axios.create({
    baseURL: b2b_variables.base_url.digital_service,
    headers: {
      "Content-Type": "application/json",
      Authorization: "JWT " + access_token,
    },
    responseType: "json",
  });

export const BUS_API = (access_token) =>
  axios.create({
    baseURL: b2b_variables.base_url.bus,
    headers: {
      "Content-Type": "application/json",
      Authorization: "JWT " + access_token,
    },
    responseType: "json",
  });

export const FLIGHT_API = (access_token) =>
  axios.create({
    baseURL: b2b_variables.base_url.flight,
    headers: {
      "Content-Type": "application/json",
      Authorization: "JWT " + access_token,
    },
    responseType: "json",
  });
