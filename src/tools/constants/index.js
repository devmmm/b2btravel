export const b2b_variables = {
  base_url: {
    business_entry:
      "https://4xj2wy1zr0.execute-api.ap-southeast-1.amazonaws.com/dev/api/v1/",
    digital_service: "https://ds.api.gemtechmyanmar.com/dev/api/v1/",
    bus: "https://busapi.gemtechmyanmar.com/dev/api/v1",
    flight: "https://flightapi.gemtechmyanmar.com/dev/api/v1/",
  },
  encrypt_key: "bqw868/SK5b0hbzd6TdWcxST9AV44eNwnAAcVTCB0P8=",
  system_font: "Cairo",
};

export const b2b_user_data = {
  access_token: localStorage.getItem("access_token"),
  refresh_token_old: localStorage.getItem("refresh_token"),
  create_time: localStorage.getItem("create_time"),
};
